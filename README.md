# p94-ts

The highly unfinished skeleton of an ambitious Roblox game.

This is a Rojo project that uses Roblox-ts. You can compile it and play it yourself if you have Rojo and Roblox-ts installed. However, I cannot guarantee that it will actually work.

(Remember to use `npm up` before attempting to edit or compile.)