import { sceneEvent, sceneEntityObject } from "./SceneManager";
const aiList = ["orb"] as const;
export type aiTypeName = typeof aiList[number];
export interface ai {
	aiTypeName: aiTypeName;
	actionInProgress?: "jello";
}
export function makeAi(aiTypeName: aiTypeName): ai {
	return { aiTypeName: aiTypeName };
}
type aiBehaviorTable = {
	[typename in aiTypeName]: (ai: ai, entities: sceneEntityObject, entityList: string[]) => [ai, sceneEvent[]];
};
const aiBehaviorList: aiBehaviorTable = {
	// ! Without "this: void", the functions compile as methods and break (this is a roblox-ts feature)
	orb: function (this: void, ai: ai, entities: sceneEntityObject, entityList: string[]): [ai, sceneEvent[]] {
		return [
			ai,
			[
				[
					"transform_entities_in_radius",
					{ type: "support", magnitude: 1000, affectsHealth: true, affectsBarrier: true },
					8.5,
				],
			],
		];
	},
} as const;
// Will take as arguments: the list of entities, workspace (implicitly)
function runAi(ai: ai, entities: sceneEntityObject, entityList: string[]): [ai, sceneEvent[]] {
	const x = aiBehaviorList[ai.aiTypeName](ai, entities, entityList);
	return x;
}
export function runAis(ais: ai[], entities: sceneEntityObject, entityList: string[]): [ai[], sceneEvent[]] {
	const aiToRun = ais.shift();
	if (aiToRun) {
		const [newAi, newSceneEvents] = runAi(aiToRun, entities, entityList);
		const [otherNewAis, otherNewSceneEvents] = runAis(ais, entities, entityList);
		return [
			[newAi, ...otherNewAis],
			[...newSceneEvents, ...otherNewSceneEvents],
		];
	} else {
		return [[], []];
	}
}
