// "PlayerManager": Handle the data of players. This involves receiving them when they arrive, cleaning up after they exit, teleporting them, etc.
// This is also where persisted data is stored while the player is playing. Communication is required between here and the scene, rather than direct access, to protect the player's data.

import { sceneRequest, stageRequest } from "./SceneManager";
export type playerActivity =
	| ["player_joined" | "player_left", Player]
	| ["player_tried_action", Player, unknown[]]
	| ["placeholder", "foo"];
export type messageForPlayer = ["placeholder", "foo"];

interface saveDataEntry {
	// + May need to move this to a dedicated archiver module/actor for data safety
	placeholder: string;
}
interface storedPlayer {
	saveData: saveDataEntry;
	state: [stateName: "inGame", currentSceneName: string] | [stateName: "inMenu"];
}
export type playerStorage = (storedPlayer | undefined)[];
function getSaveData(userId: number): saveDataEntry {
	return { placeholder: "foo" };
}
function initStoredPlayer(userId: number): storedPlayer {
	return {
		saveData: getSaveData(userId),
		state: ["inMenu"],
	};
}
function handlePlayerActivity(
	playerStorage: playerStorage,
	now: number,
	activity: playerActivity,
): [playerStorage, stageRequest[]] {
	const newPlayerStorage = [...playerStorage]; // Hopefully not slow
	const sceneMessages: stageRequest[] = [];
	if (activity[0] === "player_joined") {
		const userId = activity[1].UserId;
		newPlayerStorage[userId] = initStoredPlayer(userId);
		// + check if player has auto-load-in turned off
		sceneMessages.push(["scene0", ["load_in_player", activity[1]]]);
	} else if (activity[0] === "player_left") {
		const userId = activity[1].UserId;
		// + save data to datastore
		newPlayerStorage[userId] = undefined;
		sceneMessages.push(["scene0", ["remove_player", activity[1]]]);
	} else {
		// (unimplemented)
	}
	return [newPlayerStorage, sceneMessages];
}
export interface sceneRequestDictionary {
	[sceneName: string]: sceneRequest[] | undefined;
}
export function handleConsecutivePlayerActivities(
	playerStorage: playerStorage,
	now: number,
	activities: playerActivity[],
): [playerStorage, sceneRequestDictionary] {
	const activityToHandle = activities.shift(); // A bit destructive (slice isn't implemented yet), but it works and is honestly cleaner than the alternative
	if (activityToHandle) {
		const [newPlayerStorage, requests] = handlePlayerActivity(playerStorage, now, activityToHandle);
		const [newerPlayerStorage, sceneRequestDictionary] = handleConsecutivePlayerActivities(
			playerStorage,
			now,
			activities,
		);
		for (const [sceneName, sceneRequest] of requests) {
			//requests.forEach(function ([sceneName, sceneRequest]: stageRequest) {
			let requestsForThisScene = sceneRequestDictionary[sceneName];
			if (requestsForThisScene === undefined) {
				requestsForThisScene = [];
			}
			requestsForThisScene.push(sceneRequest);
			sceneRequestDictionary[sceneName] = requestsForThisScene;
		}
		return [newerPlayerStorage, sceneRequestDictionary];
	} else {
		return [playerStorage, {}];
	}
}
function sendToPlayer() {}
export function handleMessagesForPlayer(
	playerStorage: playerStorage,
	messagesForPlayer: messageForPlayer[],
): playerStorage {
	return playerStorage; // Will be implemented in Goal 3: Effects
}
export function initPlayerStorage() {
	return [];
}

/* Deprecated (may have useful information)
class storedPlayerHandler implements storedPlayer {
	constructor(player: Player) {
		this.player = player;
		this.inMainMenu = true;
		this.saveData = { placeholder: "fortnite" };
	}
	teleportToServer() {
		// + Do checking related to where the player is allowed to go
		// + Teleport player to other server, sending a message to have them load in automatically
	}
	setPosition(location: Vector3) {
		if (this.entity) {
			this.entity.setPosition(location);
		}
	}
	ability(ability: string, state: boolean) {
		if (this.entity) {
			this.entity.useAbility(ability, state);
		}
	}
	loadIn() {
		this.entity = makeEntity(["Character", this.player]);
		// + Give the entity the stats it's supposed to have, load from save data maybe?
	}
	player: Player;
	inMainMenu: boolean;
	// + Other data that is unique to players but does not persist between sessions
	saveData: saveDataEntry; // This gets synced with the actual datastore
	//entity?: entityController;
}*/

/*class playerStorageHandler implements playerStorage {
	constructor() {}
	initPlayer(player: Player) {
		this.playerStorageArray[player.UserId] = new storedPlayerHandler(player);
		// + Load player's datastore into server store
	}
	deinitPlayer(player: Player) {
		const entry = this.playerStorageArray[player.UserId];
		assert(entry, "Trying to remove entry of player " + player.DisplayName + ", but entry does not exist!");
		// ? Tell the entity to unload, if it still exists (the entity will tell the other clients to remove the player)
		// + Unload player's server store to datastores
		return undefined; // A nil entry to replace the entry to be wiped and maybe a success value in a wrapper
	}
	fetchPlayer(player: Player) {
		return this.playerStorageArray[player.UserId];
	}
	playerStorageArray: storedPlayerHandler[] = [];
}*/
