import { entityModifier } from "./EntityManager" 
export interface ability {
	use: (entities?: entityModifier[]) => void; // ? Does it pass back information to the entity manager that it does something with? Is an ability really just a transform of entities?
	// ... this transform would need the positions and stats of the entities involved as well as potential obstacles
}
