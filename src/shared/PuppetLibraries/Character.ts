// "Character": Tell the puppetmaster how to make and handle characters.
const RunService = game.GetService("RunService");

export function makeModel(player: Player) {
	player.LoadCharacter();
	// Get model
	let model: Model | undefined;
	const now = os.clock();
	do {
		model = player.Character;
		RunService.Heartbeat.Wait();
	} while (!model && os.clock() - now < 5);
	assert(model, 'Timed out: Model of player "' + player.DisplayName + '" failed to load!');
	// Get root part
	const root = model.WaitForChild("HumanoidRootPart", 1);
	assert(
		root && classIs(root, "Part"),
		'Timed out: Root Part of player "' + player.DisplayName + '" failed to load!',
	);
	return [model, root] as [Model, Part];
}
