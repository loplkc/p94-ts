// "Puppetmaster": Create consistent, reliable interfaces for the physical characters.
import { MODULE_SUBEXT } from "roblox-ts/out/Shared/constants";
import * as m1 from "./PuppetLibraries/Character";
const Workspace = game.GetService("Workspace");
const puppetLibraries = {
	["Character"]: m1,
	["Placeholder"]: m1,
};

export type puppet =
	| {
			puppetType: "character";
			player: Player;
			characterModel?: Model;
	  }
	| {
			puppetType: "orb";
			diameter: number;
			characterModel?: Model;
	  };

export type puppetEntry = ["Character", Player] | ["Placeholder", "Placeholder"];

/*export interface puppet {
	movePuppet: (location: Vector3) => void; // + Success value maybe?
}*/
/* interface puppetHandler extends puppet {
	entry: puppetEntry;
} */
/* interface completePuppetHandler extends puppetHandler {
	model: Model;
	rootPart: Part;
} */

export function makePuppetInWorkspace(puppet: puppet): puppet {
	if (puppet.puppetType === "character") {
		coroutine.resume(
			coroutine.create(function () {
				puppet.player.LoadCharacter();
			}),
		);
		return puppet;
	} else if (puppet.puppetType === "orb") {
		const orbSize = puppet.diameter;
		const Model = new Instance("Model");
		const orbPart = new Instance("Part");
		orbPart.Shape = Enum.PartType.Ball;
		orbPart.Material = Enum.Material.Neon;
		orbPart.Color = Color3.fromRGB(72, 72, 84);
		orbPart.Size = new Vector3(orbSize, orbSize, orbSize);
		orbPart.Position = new Vector3(0, 10 + orbSize / 2, 0);
		const decal = new Instance("Decal");
		decal.Texture = "rbxassetid://4821072198";
		decal.Parent = orbPart;
		orbPart.Parent = Model;
		Model.Parent = Workspace;
		const newPuppet = puppet;
		newPuppet.characterModel = Model;
		return newPuppet;
	} else {
		return puppet; // Orb will be implemented with Goal 1
	}
}
/*function makePuppetModel(puppetEntry: puppetEntry) {
	if (puppetEntry[0] === "Character") {
		return puppetLibraries[puppetEntry[0]].makeModel(puppetEntry[1]) as [Model, Part];
	} else {
		throw 'Invalid puppet type "' + puppetEntry[0] + '"!';
	}
}*/ // An old version of the function from when the puppet system was modular

/* function verifyPuppetExistence(puppetHandler: puppetHandler) {
	// + Making this do type checking is currently beyond me
	if (!puppetHandler.rootPart || !puppetHandler.rootPart.Parent) {
		print("Regenerating puppet!");
		[puppetHandler.model, puppetHandler.rootPart] = makePuppetModel(puppetHandler.entry);
	}
	// return true;
}*/
class puppetHandler /*implements puppet*/ {
	constructor(puppetEntry: puppetEntry) {
		this.entry = puppetEntry;
	}

	movePuppet(location: Vector3) {
		print("executing puppet move");
		// verifyPuppetExistence(this);
		if (!this.rootPart) {
			// + Remove this once you get better at typescript
			throw "Fornite";
		}
		this.rootPart.CFrame = new CFrame(location);
	}

	entry: puppetEntry;
	model?: Model;
	rootPart?: Part;
}
