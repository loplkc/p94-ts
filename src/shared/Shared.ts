// "Shared"
const ReplicatedStorage = game.GetService("ReplicatedStorage");
export const Input = ReplicatedStorage.WaitForChild("Input", 1);
export const Output = ReplicatedStorage.WaitForChild("Output", 1);

export function isUnknownTable(thing: unknown): thing is unknownTable {
	return typeIs(thing, "table");
}

/* export function makeApplyConsecutiveRequestsToObjectFunction<mainObjectType, inputRequestType, outputRequestType>(
	applyRequestToObject: (
		mainObject: mainObjectType,
		now: number,
		inputRequest: inputRequestType,
	) => [mainObjectType, outputRequestType[]],
) {
	function applyConsecutiveRequestsToObject(
		mainObject: mainObjectType,
		now: number,
		inputRequests: inputRequestType[],
	): [mainObjectType, outputRequestType[]] {
		const inputRequestToHandle = inputRequests.shift(); // A bit destructive (shift isn't implemented yet), but it works and is honestly cleaner than the alternative
		if (inputRequestToHandle !== undefined) {
			const [newMainObject, outputRequests] = applyRequestToObject(mainObject, now, inputRequestToHandle);
			const [newerMainObject, newOutputRequests] = applyConsecutiveRequestsToObject(
				newMainObject,
				now,
				inputRequests,
			);
			return [newerMainObject, [...outputRequests, ...newOutputRequests]];
		} else {
			return [mainObject, []];
		}
	}
	return applyConsecutiveRequestsToObject;
}*/
/*export class actorClass<MessageType> implements actor<MessageType> {
	message(message: MessageType) {
		this.mailbox.push(message)
		if (!this.busy) {
			this.busy = true
		}
	}
	readMessage() {
		return this.mailbox.shift()
	}
	mailbox: MessageType[] = [];
	busy = false;
}*/
