// "The": Handle events.
import { entity, entityTransformTemplate, makeEntity, entityTemplate } from "./EntityManager";
import { ai, aiTypeName, makeAi, runAis } from "./aiManager";
import { messageForPlayer } from "./PlayerManager";
export type sceneRequest =
	| [request: "load_in_player", player: Player] // More stats would go here
	| [request: "remove_player", player: Player]
	| [request: "use_ability", playerUserId: number, mousePosition: Vector3]
	| [request: "placeholder", foo: "bar"];
export type stageRequest = [scene: string, sceneRequest: sceneRequest];
export type sceneEvent =
	| [event: "entity_created", entityTemplate: entityTemplate, aiTypeName?: aiTypeName, creatorEntityName?: string]
	| [event: "entity_removed", entityName: string, destroyPuppet: boolean]
	| [
			event: "transform_entities_in_radius",
			transformTemplate: entityTransformTemplate,
			radius: number,
			entityCausingTransformName?: string,
	  ];
/*| [
			event: "entity_attacked" | "entity_healed",
			entityName: string,
			otherEntityName: string,
			transformTemplate: entityTransformTemplate,
	  ]
	| [event: "entity_died", entityName: string, killerEntityName: string];*/

type sceneTransformation = ["attack", entity, entity] | ["heal"];
type endConditionFunction = (containedEntities: entity[], timeElapsed: number) => [false] | [true, messageForPlayer[]]; // This also needs some way to contact other scenes
export interface sceneTemplate {
	readonly sceneComplete: endConditionFunction; // Checks conditions that need to pass for the scene to end (e.g. entityX.Alive == false || timeSpent > 1000) and also tells what to do about it depending on the result (victory or loss)
	entityTemplates?: [entityTemplate, aiTypeName][];
}
export interface sceneEntityObject {
	[entityName: string]: entity | undefined;
}
export interface scene {
	entities: sceneEntityObject;
	entityList: string[];
	ais: ai[];
	readonly sceneComplete: endConditionFunction;
}
export interface stage {
	[sceneName: string]: scene | undefined;
}
function addEntityToLists(
	entities: sceneEntityObject,
	entityList: string[],
	entityTemplate: entityTemplate,
): [sceneEntityObject, string[]] {
	const newEntityName = entityTemplate.id.name;
	const newEntity = makeEntity(entityTemplate);
	const newEntities = entities;
	newEntities[newEntityName] = newEntity;
	const newEntityList = [...entityList, newEntityName];
	return [newEntities, newEntityList];
}
function addAiToLists(aiList: ai[], aiTypeName: aiTypeName): ai[] {
	const newAi = makeAi(aiTypeName);
	print("Made new ai with name " + newAi.aiTypeName);
	return [...aiList, newAi];
}
export function initScene(sceneTemplate: sceneTemplate): scene {
	let newEntities: sceneEntityObject = {};
	let newEntityList: string[] = [];
	let newAis: ai[] = [];
	const entityTemplates = sceneTemplate.entityTemplates;
	if (entityTemplates) {
		for (const entityTemplate of entityTemplates) {
			[newEntities, newEntityList] = addEntityToLists(newEntities, newEntityList, entityTemplate[0]);
			newAis = addAiToLists(newAis, entityTemplate[1]);
		}
	}
	const newScene: scene = {
		entities: newEntities,
		entityList: newEntityList,
		ais: newAis,
		sceneComplete: sceneTemplate.sceneComplete,
	};
	return newScene;
}

export function processSceneInternalEvents(scene: scene, now: number): [ai[], sceneEvent[]] {
	const [newAis, sceneEvents] = runAis(scene.ais, scene.entities, scene.entityList);
	return [newAis, sceneEvents]; // Placeholder, will be implemented with Goal 1: Enemy Action
}
function processSceneExternalRequest(scene: scene, now: number, request: sceneRequest): sceneEvent[] {
	// print("Processing external request");
	if (request[0] === "load_in_player") {
		// print("loading in player");
		return [
			[
				"entity_created",
				{
					id: {
						name: "player_" + tostring(request[1].UserId),
						team: 0,
						tags: {},
					},
					maxStatuses: [100, 0],
					baseModifiers: [1, 1, 0, 0],
					baseStatusEffects: [],
					puppet: {
						puppetType: "character",
						player: request[1],
					},
				},
			],
		];
	} else if (request[0] === "remove_player") {
		return [["entity_removed", "player_" + tostring(request[1].UserId), true]];
	} else if (request[0] === "use_ability") {
		return []; // Placeholder, will be implemented with Goal 2: Player Action
	} else {
		return []; // request[0] should be type "never"
	}
}
export function processSceneExternalRequests(scene: scene, now: number, requests: sceneRequest[]): sceneEvent[] {
	const requestToHandle = requests.shift();
	if (requestToHandle) {
		const sceneEvents = processSceneExternalRequest(scene, now, requestToHandle);
		const moreSceneEvents = processSceneExternalRequests(scene, now, requests);
		return [...sceneEvents, ...moreSceneEvents];
	} else {
		return [];
	}
}
function processSceneEvent(scene: scene, now: number, event: sceneEvent): [scene, messageForPlayer[]] {
	if (event[0] === "entity_created") {
		const sceneEntities = scene.entities;
		const sceneEntityList = scene.entityList;
		const sceneAis = scene.ais;
		const [newSceneEntities, newSceneEntityList] = addEntityToLists(sceneEntities, sceneEntityList, event[1]);
		const newAiTypeName = event[2];
		if (newAiTypeName) {
			const newSceneAis = addAiToLists(sceneAis, newAiTypeName);
			scene.ais = newSceneAis;
		}
		scene.entities = newSceneEntities;
		scene.entityList = newSceneEntityList;
		return [scene, []];
	} else if (event[0] === "transform_entities_in_radius") {
		print("Magnitude: " + event[1].magnitude);
		return [scene, []];
	} else {
		return [scene, []]; // The rest will be implemented with Goal 1: Enemy Action
	}
}
export function processSceneEvents(scene: scene, now: number, events: sceneEvent[]): [scene, messageForPlayer[]] {
	const eventToProcess = events.shift();
	if (eventToProcess) {
		const [newScene, messages] = processSceneEvent(scene, now, eventToProcess);
		const [newestScene, restOfMessages] = processSceneEvents(newScene, now, events);
		return [newestScene, [...messages, ...restOfMessages]];
	} else {
		return [scene, []];
	}
}
/*export interface sceneBackstage {
	readonly entityProperties: {
		[glumph: string]: entityProperties;
	}
}*/

// export interface scene extends sceneTemplate {
/*containedScenes?: {
		[sceneName: string]: scene | undefined
	}; // Scenes within this scene that are isolated from each other and can be run in parallel 
	// Not necessarily "A list of events that need to return true (in sequence) to complete this event", but such events would go there
	*/ //Scene nesting is currently cancelled
// containedEntities: string[]; // "A list of entities that need to die to complete the event" (not really but kinda)
// containedEntityProperties: {
// [entityName: string]: entityProperties
// }
// containedEntityStatuses
// players: {
// [userId: number]: [inThisScene: true] | [inThisScene: false, subScene: string] | undefined
// }
//timeout?: number; // A timeout for the event; passes a lose condition if there are other completion requirements that have not been satisfied
// }

/*function getPlayerSceneName(scene: scene, userId: number): success<string | false> {
	let playerSceneLocation = scene.players[userId];
	if (!playerSceneLocation) {
		return [false, "Player does not exist"]; // Some kind of error needs to go here
	} else if (playerSceneLocation[0]) {
		return [true, false];
	} else {
		return [true, playerSceneLocation[1]] 
	}
}*/
/*const playerSceneResult = getPlayerSceneName(scene, request[0].UserId)
	if (!playerSceneResult[0]) {
		return [scene, []]; // Some kind of error needs to go here
	}
	const playerSceneName = playerSceneResult[1]
	if (!playerSceneName) {
		const playerEntity = getEntityByName
		if (request[1] == "useAbility") {
			return useAbility(scene, )
		} else {
			throw("Invalid request to SceneManager")
		}
	} else { // Case needs testing once it becomes relevant (this code is a mess)
		const containedScenes = scene.containedScenes
		assert(containedScenes)
		let playerScene = containedScenes[playerSceneName]
		assert(playerScene)
		const sceneRequestResult = applyRequestToScene(playerScene, now, request) // There should be no stack overflow unless you nest too many scenes
		containedScenes[playerSceneName] = sceneRequestResult[0] // This is questionably object-oriented
		scene.containedScenes = containedScenes
		return [scene, sceneRequestResult[1]]
	}*/
//}

/*export function applyRequestsToScene(
	scene: scene,
	now: number,
	requests: sceneManagerRequest[],
): success<[scene, messageForPlayer[]]> {
	try {
		let newScene: scene = scene;
		let outgoingRequests: messageForPlayer[] = [];
		requests.forEach(function (request: sceneManagerRequest) {
			const sceneRequestResult = applyRequestToScene(newScene, now, request);
			newScene = sceneRequestResult[0];
			outgoingRequests = [...outgoingRequests, ...sceneRequestResult[1]];
		});
		return [true, [newScene, outgoingRequests]];
	} catch (error) {
		return [false, error];
	}
}*/
