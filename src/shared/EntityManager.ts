// "EntityManager": Create entity objects and apply transformations to them.
// A bold (or just foolish) attempt at functional programming in a videogame entity system
import { makePuppetInWorkspace, puppet } from "./Puppetmaster";
// import { ability } from "./AbilityManager";
const entityTags = ["minion"] as const;
type modifiers = readonly [power: number, speed: number, defense: number, resistance: number]; // values used for calculation, only modified by buffs and debuffs
type statuses = readonly [health: number, barrier: number]; // values used to store an entity's current status (more existential than stats)
type statusEffect = readonly [
	name: string,
	type: string,
	strength: number,
	modifiers?: modifiers,
	effect?: entityTransformTemplate,
]; // Should also be able to cause a visual effect somehow - maybe rather than having an entityTransformTemplate, it should broadcast a message back to the top
// ...that can affect other entities or communicate with the client
type entityTag = typeof entityTags[number];
export interface entityId {
	readonly name: string;
	readonly team: number;
	readonly tags: {
		[tagName in entityTag]?: boolean;
	};
}
export interface entityProperties {
	readonly id: entityId;
	readonly maxStatuses: statuses;
	readonly baseModifiers: modifiers;
	readonly baseStatusEffects: statusEffect[]; // Permanent immunities/status effects
	readonly puppet: puppet;
}

export interface entityTemplate extends entityProperties {}

export interface entity {
	statuses: statuses;
	statusEffects: statusEffect[];
	readonly properties: entityProperties;
}

type entityTransformType = "support" | "attack";
export interface entityTransformTemplate {
	type: entityTransformType;
	magnitude: number;
	affectsHealth: boolean;
	affectsBarrier: boolean;
	statusEffectsGranted?: placeholder[]; // TBA (Stuff like burn, slow, stun, etc.)
}
type entityTransformDeterminer = (
	entityPerformingId: entityId,
	entityReceivingId: entityId,
	entityPerformingPuppet: puppet,
	entityReceivingPuppet: puppet,
) => [entityTransformType, entityTransformTemplate] | false;
interface ability {
	placeholder: entityTransformDeterminer; // TBA (Most abilities will not be a single instant of damage/heal)
}
// Immunities are just status effects that take the place of a damaging status effect (so ones of that type can't be added) but don't do anything
// Status effects have a strength value that determines whether they can override another status effect of the same type (holy inferno can replace burn but burn can't replace holy inferno)

export interface entityController {
	//Deprecated
	setPosition: (location: Vector3) => void;
	useAbility: (abilityName: string, activating: boolean) => void;
}

export interface entityModifier {} // Deprecated

export function makeEntity(entityTemplate: entityTemplate): entity {
	print("Trying to make entity");
	return {
		statuses: entityTemplate.maxStatuses,
		statusEffects: [],
		properties: {
			id: entityTemplate.id,
			maxStatuses: entityTemplate.maxStatuses,
			baseModifiers: entityTemplate.baseModifiers,
			baseStatusEffects: entityTemplate.baseStatusEffects,
			puppet: makePuppetInWorkspace(entityTemplate.puppet),
		},
	};
}
/* Prototype
	entityTransform(setOfAllEntities: entity[], actingEntity: entity, ability destructured to {hitBoxList, modificationList})
	const entitiesEligible = getAffectedEntities(setOfAllEntities, actingEntity)
	const entitiesHit = entitiesEligible ? getHitEntities(entitiesEligible, hitBoxList) : entitiesEligible // This would be a generic function that accepts an array or a single hitbox
	const entityAlteration = entitiesHit ? applyEntityToAlteration(entityTransform, actingEntity) : entitiesHit
	return modifiedEntities = entityAlteration ? alterEntity(entitiesHit, entityAlteration) : entitiesHit // entitiesHit could be an array of arrays containing entities or just an array of entities
	
	useAbilityPart(abilityPart: abilityPart, setOfAllEntities: entity[], thisEntity: entity)
	const transformedEntities = ableToUse ? entityTranform(setOfAllEntities, thisEntity) : ableToUse
	const privateMessaged = transformedEntities ? messageClient(UNIMPLEMENTED) : transformedEntities
	const globalMessaged = privateMessaged ? messageAllClients(UNIMPLEMENTED) : privateMessaged
	return [globalMessaged, transformedEntities, schedulingForNextAbilityPart] // Might need to add some kind of reason here, not just an indiscriminate error

	useAbility(now: number,  setOfAllEntities: entity[], thisEntity: entity, ability: ability)
	const completeAbility = getAbilityToUse(ability, thisEntity) // Can return either [false, error] or [true, value]
	const ableToUse = completeAbility ? canUseAbility(now, completeAbility) : completeAbility // canUseAbility should return the first abilityPart
	return resultOfFirstAbilityPart = ableToUse ? useAbilityPart(ability.firstPart, setOfAllEntities, thisEntity) : [ableToUse]
*/

// type entityTransform = (entity: entity) => entity;
// interface entityTransformTemplate {
/*
	allies: {
		minions: {
		}
	}
	opponents: {

	}
	affectsMinions: boolean,
	excludesSelf: boolean,
	affectsSameTeam: boolean,
*/ //}
// type entityTransformTemplate = [entityTransformEligibilityTemplate, entityTransformApplicationTemplate]
/*function isEligibleForTransform(entityPerformingTransform: entity, entityReceivingTransform: entity, eligibilityTemplate: entityTransformEligibilityTemplate): false | [boolean] { // This function sucks
	const entityReceivingId = entityReceivingTransform.id
	const onSameTeam = entityPerformingTransform.id.team == entityReceivingId.team
	if (onSameTeam && !eligibilityTemplate.affectsSameTeam) {
		return false
	}
	if (entityPerformingTransform == entityReceivingTransform && eligibilityTemplate.excludesSelf) {
		return false
	}
	if (entityReceivingId.isMinion && !eligibilityTemplate.affectsMinions) {
		return false
	}
	if (!eligibilityTemplate.extraFunction(entityPerformingTransform, entityReceivingTransform)) {
		return false
	}
	return [onSameTeam]
}*/

/*function applyAttackToEntityStats() {//else if (transformType === "heal") {
		// Apply receiver's status effects to incoming heal
		const newEntityStatuses = applyHealToStatuses(
			entityStats.statuses, // DRY alert
			magnitude,
			entityTransformTemplate.affectsHealth,
			entityTransformTemplate.affectsBarrier,
			maxStatuses,
		);
		// + Add or remove status effects (don't compare against immunities)
		return {
			statuses: newEntityStatuses,
			statusEffects: entityStats.statusEffects, // Placeholder
		};
	}*/
function applyAttackToEntityStatuses(
	entityStatuses: statuses,
	entityTransformTemplate: entityTransformTemplate,
	entityModifiers: modifiers,
	entityStatusEffects: statusEffect[],
): statuses {
	// + Apply status effects of receiving entity to damage (e.g. armor break)
	const modifiedDamage = applyModifiersToDamage(entityTransformTemplate.magnitude, entityModifiers);
	const newStatuses = applyDamageToStatuses(
		entityStatuses,
		modifiedDamage,
		entityTransformTemplate.affectsHealth,
		entityTransformTemplate.affectsBarrier,
	);
	return entityStatuses;
}
/*function applyHealToEntityStats() {//if (transformType === "attack") {
		// Apply receiver's status effects to incoming damage
		const incomingDamage = applyModifiersToDamage(magnitude, baseModifiers);
		const newEntityStatuses = applyDamageToStatuses(
			entityStats.statuses,
			incomingDamage,
			entityTransformTemplate.affectsHealth,
			entityTransformTemplate.affectsBarrier,
		);
		// + Add or remove status effects (compare against immunities)
		return {
			statuses: newEntityStatuses,
			statusEffects: entityStats.statusEffects, // Placeholder
		};
	} */
function applyHealToEntityStatuses(
	entityStatuses: statuses,
	entityTransformTemplate: entityTransformTemplate,
	entityModifiers: modifiers,
	entityMaxStatuses: statuses,
	entityStatusEffects: statusEffect[],
): statuses {
	// + Apply status effects of receiving entity to damage (e.g. heal up)
	const newStatuses = applyHealToStatuses(
		entityStatuses,
		entityTransformTemplate.magnitude,
		entityTransformTemplate.affectsHealth,
		entityTransformTemplate.affectsBarrier,
		entityMaxStatuses,
	);
	return entityStatuses;
}
// ? Are you meant to pipe determineEntityTransform into the resulting function?
/*function determineEntityTransform(
	entityPerformingTransformId: entityId,
	entityReceivingTransform: entityId,
): entityTransformTemplate | false {
	return false; // Placeholder
}
*/
function applyEntityToAttack(
	entityModifiers: modifiers,
	entityStatusEffects: statusEffect[],
	entityTransformTemplate: entityTransformTemplate,
): entityTransformTemplate {
	const outgoingAttack = applyModifiersToAttack(entityTransformTemplate.magnitude, entityModifiers);
	// + Apply user's status effects to outgoing attack
	// (Old; same thing) + Apply status effects of performing entity to attack (e.g. weaken)
	entityTransformTemplate.magnitude = outgoingAttack; // Mutating is cringe, but the other parts of the transform need to be defined first
	return entityTransformTemplate;
}
function applyEntityToHeal(
	entityModifiers: modifiers,
	entityStatusEffects: statusEffect[],
	entityTransformTemplate: entityTransformTemplate,
): entityTransformTemplate {
	// + Apply user's status effects to outgoing heal
	return entityTransformTemplate; // There could be a heal modifier later
}
/*function transformEntityStats(
	entityStats: entityStats,
	entityTransformTemplate: entityTransformTemplate,
	baseModifiers: modifiers,
	maxStatuses: statuses,
): entityStats {
	const magnitude = entityTransformTemplate.magnitude*/
//}
/*interface entityTransform extends entityTransformTemplate { 
	specificEntity?: string,
	team: "players" | "enemies",
}*/
type abilityTemplate = [
	[entityTransformTemplate[], number], // I guess the number is a delay until the next part or something
];
/*type ability = [
	[entityTransform[], number]
]*/
// scene, player, aimLocation
// const ability = getAbility
// const entities = applyAbilityToScene(scene, ability)

function applyDamageToHealth(health: number, damage: number): [newHealth: number, excessDamage: number] {
	// Needs testing
	const newHealth = health - damage;
	if (newHealth < 0) {
		return [0, -newHealth];
	} else {
		return [newHealth, 0];
	}
}
const applyDamageToBarrier = applyDamageToHealth;
function applyDamageToStatuses(
	statuses: statuses,
	damage: number,
	affectsHealth: boolean,
	affectsBarrier: boolean,
): statuses {
	if (affectsBarrier) {
		const [newBarrier, excessBarrierDamage] = applyDamageToBarrier(statuses[1], damage);
		const [newHealth, excessHealthDamage] = applyDamageToHealth(
			statuses[0],
			affectsHealth ? excessBarrierDamage : 0,
		);
		return [newHealth, newBarrier];
	} else if (affectsHealth) {
		const [newHealth, excessHealthDamage] = applyDamageToHealth(statuses[0], damage);
		return [newHealth, statuses[1]];
	} else {
		return statuses;
	}
}
function applyHealToHealth(
	currentHealth: number,
	heal: number,
	maxHealth: number,
): [newHealth: number, excessHeal: number] {
	const newHealth = currentHealth + heal;
	if (newHealth > maxHealth) {
		return [maxHealth, newHealth - maxHealth];
	} else {
		return [newHealth, 0];
	}
}
const applyHealToBarrier = applyHealToHealth;
function applyHealToStatuses(
	statuses: statuses,
	heal: number,
	affectsHealth: boolean,
	affectsBarrier: boolean,
	maxStatuses: statuses,
): statuses {
	if (affectsHealth) {
		const [newHealth, excessHealth] = applyHealToHealth(statuses[0], heal, maxStatuses[0]);
		const [newBarrier, excessBarrier] = applyHealToBarrier(
			statuses[1],
			affectsBarrier ? excessHealth : 0,
			maxStatuses[1],
		);
		return [newHealth, newBarrier];
	} else if (affectsBarrier) {
		// Using a branch isn't optimal, but as of writing I can't think of a better solution
		const [newBarrier, excessBarrier] = applyHealToBarrier(statuses[1], heal, maxStatuses[1]);
		return [statuses[0], newBarrier];
	} else {
		return statuses;
	}
}
function applyPowerToAttack(attack: number, power: number) {
	return attack * power;
}
function applyModifiersToAttack(attack: number, modifiers: modifiers): number {
	// Get arguments they use (a bit sketchy)
	return applyPowerToAttack(attack, modifiers[0]);
}
function applyResistanceToDamage(damage: number, resistance: number): number {
	return damage * (1 - resistance);
}
function applyDefenseToDamage(damage: number, defense: number): number {
	return damage - defense;
}
function applyModifiersToDamage(damage: number, modifiers: modifiers): number {
	return applyResistanceToDamage(applyDefenseToDamage(damage, modifiers[2]), modifiers[3]);
}
function modifiersArrayFunction(
	applyModifiersToNumber: (number: number, modifiers: modifiers) => number,
): (number: number, modifiers: modifiers[]) => number {
	return function (number: number, modifiersArray: modifiers[]) {
		let newNumber = number;
		modifiersArray.forEach(function (modifiers: modifiers) {
			newNumber = applyModifiersToNumber(newNumber, modifiers);
		});
		return newNumber;
	};
}
const applyModifiersArrayToAttack = modifiersArrayFunction(applyModifiersToAttack);
const applyModifiersArrayToDamage = modifiersArrayFunction(applyModifiersToDamage);
function getEntityByName(entityList: entity[], entityName: string): success<entity> {
	entityList.forEach(function (entity: entity) {
		if (entity.properties.id.name === entityName) {
			// Chained method calls are cringe
			return [true, entity];
		}
	});
	return [false, "Entity not found"];
}
function getAbility() {}
function applyEntityTransformToEntityStatuses(
	entityTransform: entityTransformTemplate,
	statuses: statuses,
	maxStatuses: statuses,
) {}
function applyEntityTransformToEntityList(
	entityList: entity[],
	entityTransformDeterminer: entityTransformDeterminer,
	entityPerformingTransform: entity,
	aim: Vector3,
): [entity[], placeholder[]?] {
	const entityPerformingTransformModifiers = entityPerformingTransform.properties.baseModifiers;
	const entityPerformingTransformStatusEffects = entityPerformingTransform.statusEffects; // Not good
	const newEntityList = entityList.map(function (entityReceivingTransform: entity): entity {
		const entityTransform = entityTransformDeterminer(
			entityPerformingTransform.properties.id,
			entityReceivingTransform.properties.id,
			entityPerformingTransform.properties.puppet,
			entityReceivingTransform.properties.puppet,
		);
		if (entityTransform) {
			const entityStatuses = entityPerformingTransform.statuses;
			const [entityTransformType, entityTransformTemplate] = entityTransform;
			if (entityTransformType === "attack") {
				const outgoingTransformTemplate = applyEntityToAttack(
					entityPerformingTransformModifiers,
					entityPerformingTransformStatusEffects,
					entityTransformTemplate,
				);
				const newEntityStatuses = outgoingTransformTemplate
					? applyAttackToEntityStatuses(
							entityStatuses,
							entityTransformTemplate,
							entityReceivingTransform.properties.baseModifiers,
							entityReceivingTransform.statusEffects, // This is also cringe
					  )
					: entityStatuses; // Also cringe
			} else {
				assert(entityTransformType === "support");
				const outgoingTransformTemplate = applyEntityToHeal(
					// Lots of DRY violations here
					entityPerformingTransformModifiers,
					entityPerformingTransformStatusEffects,
					entityTransformTemplate,
				);
				const newEntityStatuses = outgoingTransformTemplate
					? applyHealToEntityStatuses(
							entityStatuses,
							entityTransformTemplate,
							entityReceivingTransform.properties.baseModifiers,
							entityReceivingTransform.properties.maxStatuses,
							entityReceivingTransform.statusEffects, // So much cringe
					  )
					: entityStatuses;
			}
			return entityReceivingTransform; // Placeholder, will be implemented properly in Goal 1: Enemy Action
		} else {
			return entityReceivingTransform;
		}
	});
	return [newEntityList];
}
/*class entityHandler implements entityController {
	constructor(baseStats: stats, baseAmounts: amounts, puppetEntry: puppetEntry) {
		this.baseStats = baseStats;
		this.baseAmounts = baseAmounts;
		this.puppet = makePuppet(puppetEntry);
	}
	setPosition(location: Vector3) {
		this.puppet.movePuppet(location);
	}
	canUseAbility(abilityName: string): boolean {
		// ! Not concurrency safe
		// + buncha status checks
		const cooldown = this.cooldowns[abilityName];
		if (cooldown) {
			const now = os.clock();
			if (now - cooldown[0] < cooldown[1]) {
				return false
			}
		}
		return true;
	}
	useAbility(abilityName: string, activating: boolean) {
		const abilities = this.abilities;
		if (activating) {
			if (this.canUseAbility(abilityName)) {
				const ability = abilities[abilityName];
				if (ability != undefined) {
					const abilityResult = ability.use();
				}
			}
		} else {
			// + Ability cancellation - perhaps the useAbility inside the entity returns a function to store that the ability watches that ceases the ability when executed
		}
		// Blah blah blah
	}

	puppet: puppet;
	abilities: {
		[key: string]: ability | undefined;
	} = {};
	cooldowns: {
		[key: string]: [start: number, length: number] | undefined;
	} = {};
	baseStats: stats;
	baseAmounts: amounts;
}*/
/*
export function makeEntity(
	puppetEntry: puppetEntry,
	baseStats = [100, 1, 16, 0] as stats,
	baseAmounts = [100, 0] as amounts,
) {
	return new entityHandler(baseStats, baseAmounts, puppetEntry);
}

class entityManager extends actorClass<entityManagerRequest> {
	constructor() {
		super()
	}
	entities: entity[] = [];
}
export function initEntityManager() {
	return new entityManager();
}
*/
