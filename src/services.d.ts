/*
type bodyPart = "root" | "torso" | "head" | "leftArm" | "rightArm" | "leftLeg" | "rightLeg";
type serverMessageType = "init" | "promptError" | "enterGame";
type clientMessageType = "move" | "placeholder";
// + Why can the client see all of these?

type meshType = "Ball";
type effectState = [CFrame, Vector3, Color3, number]; // The number is transparency
type effectEntry = [meshType, EnumItem, effectState[]]; // The enumitem is material

*/
type unknownTable = { [numberKey: number]: unknown; [stringKey: string]: unknown };
type success<Wrapped> = [true, Wrapped] | [false, string];
type placeholder = "foo";
/*interface hookInEntry {
	name: string;
	guiObject: GuiObject;
}*/
/*interface runningEvent {
	endTime?: number;
}*/
