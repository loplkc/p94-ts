import { sceneTemplate } from "shared/SceneManager";
import { entityTemplate } from "shared/EntityManager";
import { aiTypeName } from "shared/aiManager";
const entityTemplates: [entityTemplate, aiTypeName][] = [];
for (let i = 0; i < 1; i += 1) {
	entityTemplates.push([
		{
			id: {
				name: "orb" + tostring(i),
				team: 1,
				tags: {},
			},
			maxStatuses: [100, 0],
			baseModifiers: [1, 1, 0, 0],
			baseStatusEffects: [],
			puppet: {
				puppetType: "orb",
				diameter: 16,
			},
		},
		"orb",
	]);
}
export const scene0Template: sceneTemplate = {
	sceneComplete() {
		return [false];
	},
	entityTemplates: entityTemplates,
};
