// "init": The main client-side thread.
const Players = game.GetService("Players");
const RunService = game.GetService("RunService");
import { isUnknownTable } from "shared/Shared";
import { bindToServerMessage, messageServer } from "./ClientMessenger";
import { handleGuiInput, drawGui, closeGui } from "./GuiHandler";
import { makeEffectRunner, effectRunner } from "./EffectMaker";
import { makeActionBinder, actionBinder, translateInputState } from "./InputHandler";
const LOCALPLAYER = Players.LocalPlayer;
const PLAYERGUI = LOCALPLAYER.WaitForChild("PlayerGui", 1) as PlayerGui;
assert(
	PLAYERGUI && classIs(PLAYERGUI, "PlayerGui"),
	'PlayerGui of "' + LOCALPLAYER.DisplayName + '"does not exist! (HOW???)',
);
let inMainMenu = true;

function openMainMenu(playerGui: PlayerGui) {
	messageServer("placeholder", "OpeningMainMenu"); // > Server should check if there are entities to clean up + new messagetype?
	const mainMenuButtons = drawGui(playerGui, "MainMenu");
	for (const mainMenuButton of mainMenuButtons) {
		mainMenuButton[0].Activated.Connect(function () {
			handleGuiInput(messageServer, mainMenuButton[1][0], mainMenuButton[1][1]);
		});
	}
}

function handlePlayerAction(action: string, state: Enum.UserInputState, inputObject: InputObject) {
	messageServer("PlayerAction", [action, translateInputState(state)]);
}

function handleGuiAction() {}

function handleServerMessage(messageType: unknown, messageContent: unknown) {
	if (messageType === "init") {
		openMainMenu(PLAYERGUI);
		inMainMenu = true;
		mainActionBinder.bindFunctionsToActions([["clicker1", handleGuiAction]]);
	} else if (messageType === "enterGame") {
		closeGui(PLAYERGUI, "MainMenu");
		inMainMenu = false;
		mainActionBinder.unbindFunctionsFromActions(["clicker1"]);
		mainActionBinder.bindFunctionsToActions([
			["clicker1", handlePlayerAction],
			["special1", handlePlayerAction],
			["special2", handlePlayerAction],
		]);
	} else if (messageType === "bindActions") {
		if (isUnknownTable(messageContent)) {
			mainActionBinder.assignInputsToActions(messageContent);
		}
	}
}
// Bind functions

const effectRunners: effectRunner[] = [];
// + Put stuff in the effectRunners table
RunService.RenderStepped.Connect(function (deltaTime) {
	effectRunners.forEach((effectRunner) => {
		effectRunner.runEffects(deltaTime);
	});
});

const mainActionBinder: actionBinder = makeActionBinder();

bindToServerMessage(handleServerMessage);
