// "GuiHandler": Handle Gui.
const tweenService = game.GetService("TweenService");

type supportedGuiElements =
	| "Frame"
	| "ScrollingFrame"
	| "ViewportFrame"
	| "TextLabel"
	| "ImageLabel"
	| "TextButton"
	| "ImageButton"
	| "TextBox";

interface guiStyleElement {
	BackgroundTransparency?: number;
	BackgroundColor?: Color3;
	TextScaled?: boolean;
	TextColor?: Color3;
	Font?: Enum.Font;
}

type onClickEntry = [[string, string, string?], string?];

interface guiElementEntry extends guiStyleElement {
	Type: supportedGuiElements;
	Style: guiStyle;
	Position: UDim2;
	Size: UDim2;
	Text?: string;
	onClick?: onClickEntry;
	children?: guiElementEntry[];
}

type guiStyle = {
	default: {
		BackgroundTransparency: number;
		BackgroundColor: Color3;
	};
} & {
	[guiElementName in supportedGuiElements]?: guiStyleElement;
};

interface guiStyleTable {
	[guiStyle: string]: guiStyle;
}

interface guiEntry {
	position?: UDim2;
	size?: UDim2;
	color?: Color3;
	transparency: number;
	pages: guiElementEntry[][];
	closeFunction: (baseFrame: Frame) => void;
}

interface guiTable {
	[gui: string]: guiEntry;
}

const stylesTable: guiStyleTable = {
	placeholder: {
		default: {
			BackgroundTransparency: 1,
			BackgroundColor: Color3.fromRGB(0, 0, 0),
		},
		TextLabel: {
			TextColor: Color3.fromRGB(255, 255, 255),
			Font: Enum.Font.Ubuntu,
			TextScaled: true,
		},
		TextButton: {
			BackgroundTransparency: 0.9,
			BackgroundColor: Color3.fromRGB(255, 255, 255),
			TextColor: Color3.fromRGB(255, 255, 255),
			Font: Enum.Font.Ubuntu,
			TextScaled: true,
		},
	},
};

const guiTable: guiTable = {
	MainMenu: {
		transparency: 0,
		pages: [
			[
				{
					Type: "TextLabel",
					Style: stylesTable["placeholder"],
					Position: new UDim2(0, 0, 0.175, 0),
					Size: new UDim2(1, 0, 0.15, 0),
					Font: Enum.Font.Garamond,
					Text: "Placeholder",
				},
				{
					Type: "TextButton",
					Style: stylesTable["placeholder"],
					Position: new UDim2(0.375, 0, 0.5, 0),
					Size: new UDim2(0.25, 0, 0.125, 0),
					Text: "Play",
					onClick: [["MessageServer", "EnterGame"]], // The main menu should disappear in response
				},
				{
					Type: "TextButton",
					Style: stylesTable["placeholder"],
					Position: new UDim2(0.375, 0, 0.665, 0),
					Size: new UDim2(0.25, 0, 0.125, 0),
					Text: "Settings",
				},
				{
					Type: "TextButton",
					Style: stylesTable["placeholder"],
					Position: new UDim2(0.375, 0, 0.83, 0),
					Size: new UDim2(0.25, 0, 0.125, 0),
					Text: "Changelog",
				},
			],
		],
		closeFunction: function (this: void, baseFrame: Frame) {
			print(baseFrame);
			tweenService
				.Create(baseFrame, new TweenInfo(0.5, Enum.EasingStyle.Quad, Enum.EasingDirection.Out), {
					Position: new UDim2(1, 0, 0, 0),
				})
				.Play();
			wait(1);
		},
	},
};

export function handleGuiInput(
	messageServerFunction: Callback,
	dataModification: [string, string, string?], // Should be a clientMessageType but I cannot be bothered
	guiReaction?: string,
) {
	if (dataModification[0] === "MessageServer") {
		messageServerFunction(dataModification[1], dataModification[2]);
	}
	if (guiReaction === "erase") {
		// Gui should have an unload function
	}
}

function configureTextElement(
	elementEntry: guiElementEntry,
	elementStyle: guiStyleElement | undefined,
	guiElement: TextLabel | TextButton | TextBox,
) {
	guiElement.Text = elementEntry.Text || ""; // eslint-disable-line
	guiElement.Font = elementEntry.Font || elementStyle?.Font || Enum.Font.Arial; // eslint-disable-line
	guiElement.TextColor3 = elementEntry.TextColor || elementStyle?.TextColor || new Color3(0, 0, 0);
	guiElement.TextScaled = elementEntry.TextScaled || elementStyle?.TextScaled || false;
}

function drawGuiElement(elementEntry: guiElementEntry, objectsToReturn: [TextButton | ImageButton, onClickEntry][]) {
	print("fortnite");
	const elementType = elementEntry["Type"];
	const elementStyle = elementEntry["Style"][elementType];
	const elementStyleDefault = elementEntry["Style"].default;
	const guiElement = new Instance(elementType);
	guiElement.BackgroundTransparency =
	elementEntry.BackgroundTransparency || // eslint-disable-line
		elementStyle?.BackgroundTransparency ||
		elementStyleDefault.BackgroundTransparency;
	guiElement.BackgroundColor3 =
		elementEntry.BackgroundColor || elementStyle?.BackgroundColor || elementStyleDefault.BackgroundColor;
	guiElement.Position = elementEntry.Position;
	guiElement.Size = elementEntry.Size;
	if (classIs(guiElement, "TextLabel") || classIs(guiElement, "TextBox") || classIs(guiElement, "TextButton")) {
		configureTextElement(elementEntry, elementStyle, guiElement);
	}
	if (elementEntry.children) {
		for (const subGuiElementEntry of elementEntry.children) {
			const subGuiElement = drawGuiElement(subGuiElementEntry, objectsToReturn);
			subGuiElement.Parent = guiElement;
		}
	}
	if (elementEntry.onClick) {
		assert(
			classIs(guiElement, "TextButton") || classIs(guiElement, "ImageButton"),
			'Property "OnClick" should not exist on non-button instance!',
		); // Temporary?
		objectsToReturn.push([guiElement, elementEntry.onClick]);
	}
	return guiElement;
}

export function drawGui(PLAYERGUI: PlayerGui, guiName: "MainMenu") {
	print("opening gui");
	const objectsToReturn: [TextButton | ImageButton, onClickEntry][] = [];
	const guiEntry = guiTable[guiName];
	assert(guiEntry, 'Error in Gui Handler: Gui "' + guiName + " not found!");
	// Actual Gui creation
	const screenGui = new Instance("ScreenGui");
	screenGui.Name = guiName;
	screenGui.IgnoreGuiInset = true;
	screenGui.ResetOnSpawn = false; // ?
	screenGui.Parent = PLAYERGUI;
	const baseFrame = new Instance("Frame");
	baseFrame.Name = "baseFrame";
	baseFrame.BackgroundTransparency = guiEntry.transparency;
	baseFrame.BackgroundColor3 = guiEntry.color || Color3.fromRGB(0, 0, 0);
	baseFrame.Position = guiEntry.position || new UDim2(0, 0, 0, 0);
	baseFrame.Size = guiEntry.size || new UDim2(1, 0, 1, 0);
	for (const guiElementEntry of guiEntry.pages[0]) {
		const guiElement = drawGuiElement(guiElementEntry, objectsToReturn);
		guiElement.Parent = baseFrame;
	}
	baseFrame.Parent = screenGui;
	screenGui.Parent = PLAYERGUI;
	return objectsToReturn;
}

export function closeGui(PLAYERGUI: PlayerGui, guiName: "MainMenu") {
	const screenGui = PLAYERGUI.WaitForChild(guiName, 1);
	assert(screenGui, 'ScreenGui "' + guiName + '" does not exist!');
	const baseFrame = screenGui.WaitForChild("baseFrame", 1);
	assert(baseFrame && classIs(baseFrame, "Frame"), 'Baseframe of "' + guiName + '" does not exist!');
	const closeFunction = guiTable[guiName].closeFunction(baseFrame);
	screenGui.Destroy();
}
