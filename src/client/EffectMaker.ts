const TweenService = game.GetService("TweenService");
export type effectKeypoint = [
	time: number,
	color: Color3,
	size: Vector3,
	cframe: CFrame,
	transparency: number,
	easingStyle?: Enum.EasingStyle,
	easingDirection?: Enum.EasingDirection,
];
type effect = [number, effectKeypoint[], MeshPart, number]; // Time since last keypoint, effect keypoints, effect meshPart, effect priority

export interface effectMaker {
	meshPartEffect: (
		meshPart: MeshPart,
		material: Enum.Material,
		effectKeypoints: effectKeypoint[],
		priority?: number,
	) => void;
	particleEffect: () => void;
}

export interface effectRunner {
	runEffects: (timeSinceLastFrame: number) => void;
}
/*
interface effectHandler extends effectMaker, effectRunner {
	EFFECT_FOLDER: Folder;
	effectsToRun: effect[]
}
*/
class effectHandler implements effectMaker, effectRunner {
	constructor(effectFolder: Folder) {
		this.EFFECT_FOLDER = effectFolder;
	}
	meshPartEffect(meshPart: MeshPart, material: Enum.Material, effectKeypoints: effectKeypoint[], priority?: number) {
		const effectMeshPart = meshPart.Clone();
		effectMeshPart.Material = material;
		effectMeshPart.Color = effectKeypoints[0][1];
		effectMeshPart.Size = effectKeypoints[0][2];
		effectMeshPart.CFrame = effectKeypoints[0][3];
		effectMeshPart.Transparency = effectKeypoints[0][4];
		let effectDuration = 0;
		effectKeypoints.forEach((effectKeypoint) => {
			effectDuration += effectKeypoint[0];
		});
		effectMeshPart.CastShadow = false;
		effectMeshPart.CanCollide = false;
		effectMeshPart.Anchored = true;
		effectMeshPart.Parent = this.EFFECT_FOLDER;
		// Insert the effect before the effect that will end after it
		const effectsToRun = this.effectsToRun;
		for (let index = 0; index < effectsToRun.size(); index += 1) {
			const effectInArray = effectsToRun[index];
			let effectInArrayDuration = -effectInArray[0];
			effectInArray[1].forEach((effectKeypoint) => {
				effectInArrayDuration += effectKeypoint[0];
			});
			if (effectInArrayDuration > effectDuration) {
				effectsToRun.insert(index, [0, effectKeypoints, effectMeshPart, priority !== undefined || 0] as effect);
				break;
			}
		}
		effectsToRun.insert(effectsToRun.size(), [
			0,
			effectKeypoints,
			effectMeshPart,
			priority !== undefined || 0,
		] as effect);
	}
	particleEffect() {}
	runEffects(timeSinceLastFrame: number) {
		const effectsToRun = this.effectsToRun;
		print(tostring(effectsToRun.size()) + " effects to run.");
		for (const effect of effectsToRun) {
			// Update the effect time
			let timeSinceLastKeypoint = effect[0] + timeSinceLastFrame;
			let nextKeypoint = effect[1][1];
			let timeOfNextKeypoint = nextKeypoint[0];
			while (timeSinceLastKeypoint >= timeOfNextKeypoint) {
				if (effect[1][2]) {
					timeSinceLastKeypoint -= timeOfNextKeypoint;
					effect[1].remove(0);
					nextKeypoint = effect[1][1];
					timeOfNextKeypoint = nextKeypoint[0];
				} else {
					effect[2].Destroy();
					effectsToRun.remove(0);
					continue; // Move on if this effect is done
				}
			}
			effect[0] = timeSinceLastKeypoint;
			const currentKeypoint = effect[1][0];
			const easingStyle = currentKeypoint[5];
			const meshPart = effect[2];
			// Get the alpha
			let alpha = 0;
			if (easingStyle) {
				alpha = TweenService.GetValue(
					timeSinceLastKeypoint / timeOfNextKeypoint,
					easingStyle,
					currentKeypoint[6] || Enum.EasingDirection.InOut,
				);
			} else {
				alpha = timeSinceLastKeypoint / timeOfNextKeypoint;
			}
			// Alter the effect
			meshPart.Color = currentKeypoint[1].Lerp(nextKeypoint[1], alpha);
			meshPart.Position = currentKeypoint[2].Lerp(nextKeypoint[2], alpha);
			meshPart.CFrame = currentKeypoint[3].Lerp(nextKeypoint[3], alpha);
			meshPart.Transparency = currentKeypoint[4] + (nextKeypoint[4] - currentKeypoint[4]) * alpha;
		}
		//this = effectsToRun;
	}

	EFFECT_FOLDER: Folder;
	effectsToRun: effect[] = [];
}

export function makeEffectRunner(effectFolder: Folder): effectRunner {
	return new effectHandler(effectFolder);
}
/*
local function horseEffModule(o,typ,a1,a2,a3,a4,a5,a6,a7)
	return {
		Color = a1,
		Size = a2*typ[2],
		["CFrame"] = (type(a3) == "table" and o.CFrame*a3[1]) or a3,
		Transparency = a4
	}
end

--sequence entry format:
--{color,size,cframe,transparency,time to next,easingstyle,easingdirection}
--horseeffect "typ" takes a table with a meshpart as the first value and a scale vector3 as the second
local function horseEff(typ,material,sequence) --I understand if you don't use this function. Not everyone is cool enough.
	wrap(function()
		local o = typ[1]:Clone()
		o.CastShadow = false
		o.Anchored = true
		o.CanCollide = false
		o.Material = material
		o.Color = sequence[1][1]
		o.Size = sequence[1][2]*typ[2]
		o.CFrame = sequence[1][3]
		o.Transparency = sequence[1][4]
		o.Parent = efFolder or workspace
		for i = 2, #sequence do
			local g = horseEffModule(o,typ,unpack(sequence[i])) -- goes and gets the information to be tweened to with less table indices
			local tinfo = TweenInfo.new(sequence[i-1][5] or sequence[1][5],sequence[i-1][6] or sequence[1][6],sequence[i-1][7] or sequence[1][7])
			play(tweenServiceCreate(twS,o,tinfo,g))
			wait(sequence[i-1][5] or sequence[1][5])
		end
		destroy(o)
	end)()
end
*/
