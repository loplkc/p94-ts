const Players = game.GetService("Players");
const UserInputService = game.GetService("UserInputService");
const ContextActionService = game.GetService("ContextActionService");
const Workspace = game.GetService("Workspace");
const CAMERA = Workspace.CurrentCamera as Camera;
assert(CAMERA, 'Camera of "' + Players.LocalPlayer.DisplayName + '"does not exist! (HOW???)');

function enumTypeIs<EnumAsType>(value: unknown, EnumAsObject: Enum): value is EnumAsType {
	if (typeIs(value, "EnumItem")) {
		return value.Name in EnumAsObject;
	} else {
		return false;
	}
}
type validInput = Enum.KeyCode; // + Include controller "keys"
function isValidInput(value: unknown): value is validInput {
	return enumTypeIs<Enum.KeyCode>(value, Enum.KeyCode);
}
const actionList = [
	"clicker1", // What is used to click on things (enemies in game, UI elements)
	"diamond1", // Diamond controls
	"diamond2",
	"diamond3",
	"diamond4",
	"special1", // Special controls
	"special2",
] as const;
type action = typeof actionList[number]
export type actionAssignments = {
	[actionName in action]?: validInput;
};
type actionBinding = [action, (actionName: string, state: Enum.UserInputState, inputObject: InputObject) => void];

function getMouseLocation(filterDescendantsInstances: Instance[]): [Vector3, Vector3, Instance | undefined] {
	// May be unnecessary
	const hitParams = new RaycastParams();
	hitParams.FilterType = Enum.RaycastFilterType.Blacklist;
	hitParams.FilterDescendantsInstances = filterDescendantsInstances;
	const mouseLocation = UserInputService.GetMouseLocation();
	const unitRay = CAMERA.ViewportPointToRay(mouseLocation.X, mouseLocation.Y);
	const cast = Workspace.Raycast(unitRay.Origin, unitRay.Direction.mul(1000), hitParams);
	if (cast) {
		return [cast.Position, cast.Normal, cast.Instance];
	} else {
		return [unitRay.Origin.add(unitRay.Direction.mul(1000)), new Vector3(0, 0, 0), undefined];
	}
}

export function translateInputState(state: unknown) {
	//	if (enumTypeIs<Enum.UserInputState>(state, Enum.UserInputState)) {
	if (state === Enum.UserInputState.Begin) {
		return true;
	} else if (state === Enum.UserInputState.End) {
		return false;
	} else {
		warn("Non-standard UserInputState");
		return false;
	}
	//	}
}

export interface actionBinder {
	assignInputsToActions: (actionAssignments: unknownTable) => void; // The client gets these from the server (the player saves them to datastores)
	bindFunctionsToActions: (actionBindings: actionBinding[]) => void;
	unbindFunctionsFromActions: (actions: action[]) => void;
}

class actionHandler implements actionBinder {
	// + Needs a semaphore if concurrency issues arise
	constructor() {
		// Fortnite
	}
	assignInputsToActions(actionAssignments: unknownTable) {
		const newActionAssignments: actionAssignments = {};
		actionList.forEach((action) => {
			const input: unknown = actionAssignments[action];
			if (isValidInput(input)) {
				newActionAssignments[action] = input;
			}
		});
	}
	bindFunctionsToActions(actionBindings: actionBinding[]) {
		const actionAssignments = this.actionAssignments;
		const boundActions = this.boundActions;
		if (actionAssignments) {
			actionBindings.forEach((actionBinding) => {
				const action = actionBinding[0];
				const input = actionAssignments[action];
				if (!boundActions[action] && input) {
					boundActions[action] = true;
					ContextActionService.BindAction(action, actionBinding[1], false, input);
				} else {
					// ???
				}
			});
		}
	}
	unbindFunctionsFromActions(actions: action[]) {
		const boundActions = this.boundActions;
		actions.forEach((action) => {
			if (boundActions[action]) {
				boundActions[action] = undefined;
				ContextActionService.UnbindAction(action);
			} else {
				// ???
			}
		});
	}

	actionAssignments?: actionAssignments;
	boundActions: { [action: string]: boolean | undefined } = {};
}

export function makeActionBinder(): actionBinder {
	return new actionHandler();
}
/*
function handleInput(input: InputObject, otherInteraction: boolean) {
	let mousePosition: Vector3, mouseNormal: Vector3, mouseInstance: Instance | undefined;
	[mousePosition, mouseNormal, mouseInstance] = getMouseLocation(); // eslint-disable-line prefer-const
	if (input.UserInputType === Enum.UserInputType.MouseButton1) {
		messageServer("move", mousePosition);
	}
}
*/

// UserInputService.InputBegan.Connect(handleInput);
