import { Input, Output } from "shared/Shared";
export function bindToServerMessage(functionToBind: Callback) {
	assert(Output?.IsA("RemoteEvent"), 'Remote event "Input" is of incorrect class or nil');
	Output.OnClientEvent.Connect(functionToBind);
}
export function messageServer(messageType: string, messageContent?: unknown) {
	assert(Input?.IsA("RemoteEvent"), 'Remote event "Output" is of incorrect class or nil');
	Input.FireServer(messageType, messageContent);
}
