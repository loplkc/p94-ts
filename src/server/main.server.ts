// "main": Initializes all state and handles the real world.
const VERBOSE = true;
if (!VERBOSE) {
	// This doesn't compile to something that works as intended, but I'll deal with that later
	function print() {}
	function warn() {}
}
const Players = game.GetService("Players"); // This should be the only place on the server where the Players service is mentioned
const RunService = game.GetService("RunService");
import { isUnknownTable } from "shared/Shared";
import { bindToClientMessage, messageClient, messageAllClients } from "./ServerMessenger";
// Please note: This should not use any of the properties of "scene" or "playerStorage" (it only needs to know that they exist)
import {
	stage,
	initScene,
	processSceneExternalRequests,
	processSceneInternalEvents,
	processSceneEvents,
} from "shared/SceneManager";
import {
	sceneRequestDictionary,
	playerStorage,
	initPlayerStorage,
	handleConsecutivePlayerActivities,
	handleMessagesForPlayer,
	playerActivity,
	messageForPlayer,
} from "shared/PlayerManager";
import { scene0Template } from "game/scene0";
// Initialize all state
let globalPlayerStorage: playerStorage = initPlayerStorage();
const stage: stage = {};
const listOfScenes: string[] = [];
stage["scene0"] = initScene(scene0Template);
listOfScenes.push("scene0");
// Handle the real world (TERA I/O)
let playerManagerMailbox: playerActivity[] = [];
function messagePlayerManager(message: playerActivity): void {
	playerManagerMailbox.push(message);
}
Players.PlayerAdded.Connect(function (player: Player) {
	messagePlayerManager(["player_joined", player]);
});
Players.PlayerRemoving.Connect(function (player: Player) {
	messagePlayerManager(["player_left", player]);
});
let playerAttemptedActionCounter: number[] = [];
bindToClientMessage(function (player: Player, ...messageContents: unknown[]) {
	const userId = player.UserId;
	playerAttemptedActionCounter[userId] += 1;
	if (playerAttemptedActionCounter[userId] <= 2) {
		messagePlayerManager(["player_tried_action", player, messageContents]);
	}
});

// Run everything sequentially each frame to avoid concurrency issues
let busy = false;
let frameCounter = 0;
RunService.Heartbeat.Connect(function (delta: number) {
	if (!busy) {
		busy = true;
		if (frameCounter < 10) {
			frameCounter += 1;
		} else {
			frameCounter = 0;
			playerAttemptedActionCounter = [];
		}
		const now = os.clock();
		const playerManagerMail = playerManagerMailbox;
		playerManagerMailbox = [];
		if (playerManagerMail.size() > 5) {
			warn("playerManagerMail received in a single frame exceeds 5");
		}
		let sceneRequestDictionary: sceneRequestDictionary = {};
		const messagesForPlayer: messageForPlayer[] = [];
		// Players act first
		[globalPlayerStorage, sceneRequestDictionary] = handleConsecutivePlayerActivities(
			globalPlayerStorage,
			now,
			playerManagerMail,
		);
		for (const sceneName of listOfScenes) {
			const thisScene = stage[sceneName];
			const requestsForThisScene = sceneRequestDictionary[sceneName];
			if (VERBOSE && requestsForThisScene) {
				print("Requests for this scene total " + tostring(requestsForThisScene.size()));
				requestsForThisScene.forEach(function (value) {
					print(value[0]);
				});
			}
			if (thisScene !== undefined) {
				const externalSceneEvents = requestsForThisScene
					? processSceneExternalRequests(thisScene, now, requestsForThisScene)
					: [];
				const [ais, internalSceneEvents] = processSceneInternalEvents(thisScene, now);
				thisScene.ais = ais;
				const allSceneEvents = [...externalSceneEvents, ...internalSceneEvents];
				const [newScene, newMessagesForPlayer] = processSceneEvents(thisScene, now, allSceneEvents);
				stage[sceneName] = newScene;
				for (const newMessageForPlayer of newMessagesForPlayer) {
					messagesForPlayer.push(newMessageForPlayer);
				}
			}
		}
		// Inform the players about what has just occurred
		globalPlayerStorage = handleMessagesForPlayer(globalPlayerStorage, messagesForPlayer);
		busy = false;
	} else {
		warn("Heartbeat invoked while main loop busy");
	}
});
//const playerManager: actor<playerManagerRequest> = initPlayerManager(eventManager);
/* function addPlayer(player: Player) {
	mainPlayerStorage.initPlayer(player);
	messageClient(player, "init", "idk");
}
function removePlayer(player: Player) {
	mainPlayerStorage.deinitPlayer(player);
}*/
// function handleClientMessage(player: Player, messageType: unknown, messageContent: unknown) {
//playerManager.message(["PlayerInput"])
/*
	const storedPlayer = mainPlayerStorage.fetchPlayer(player);
	if (messageType === "EnterGame") {
		try {
			storedPlayer.loadIn();
			messageClient(player, "enterGame");
		} catch (thrownError) {
			if (typeIs(thrownError, "string")) {
				const errorMessage: string =
					'Error when creating entity of "' + player.DisplayName + '": ' + thrownError;
				warn(errorMessage);
				messageClient(player, "promptError", errorMessage);
			}
		}
	} else if (messageType === "PlayerAction") {
		if (isUnknownTable(messageContent)) {
			const action = messageContent[0];
			const state = messageContent[1];
			if (typeIs(action, "string") && typeIs(state, "boolean")) {
				storedPlayer.ability(action, state);
			}
		}
	} else if (messageType === "move") {
		if (typeIs(messageContent, "Vector3")) {
			storedPlayer.setPosition(messageContent);
		}
	}
	*/
// }
