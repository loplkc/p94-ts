import { Input, Output } from "shared/Shared";
export function bindToClientMessage(functionToBind: (player: Player, ...messageContents: unknown[]) => void) {
	assert(Input?.IsA("RemoteEvent"), 'Remote event "Input" is of incorrect class or nil');
	Input.OnServerEvent.Connect(functionToBind);
}
export function messageClient(client: Player, messageType: string, messageContent?: string) {
	assert(Output?.IsA("RemoteEvent"), 'Remote event "Output" is of incorrect class or nil');
	Output.FireClient(client, messageType, messageContent);
}
export function messageAllClients(messageType: string, messageContent?: string) {
	assert(Output?.IsA("RemoteEvent"), 'Remote event "Output" is of incorrect class or nil');
	Output.FireAllClients(messageType, messageContent);
}
