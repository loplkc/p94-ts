
--Setup and Variables

--Constant variables
local plrs = game:GetService("Players")
local wS = game.Workspace
local twS = game:GetService("TweenService")
local runService = game:GetService("RunService")

local paris = ipairs
local tn = tonumber
local tSt = tostring
local unpack = unpack -- you can thank b1ooq for this one
local pcall = pcall

local rgb = Color3.fromRGB
local hsv = Color3.fromHSV

local sub = string.sub

local tFind = table.find
local insert = table.insert

local new = Instance.new
local uD2 = UDim2.new
local v3 = Vector3.new
local cF = CFrame.new
--local nSK = NumberSequenceKeypoint.new

local tweenServiceCreate = twS.Create
local play = new("Tween").Play
local destroy = game.Destroy

local xyz = CFrame.fromEulerAnglesXYZ
local wrap = coroutine.wrap

local floor = math.floor
local ceil = math.ceil
local abs = math.abs
local cos = math.cos
local sin = math.sin
local rad = math.rad
local rand = math.random
local clamp = math.clamp

local zF = cF(0,0,0)

--End of universal constant variables

local uIS = game:GetService("UserInputService")
--local uISLockedCenter = Enum.MouseBehavior.LockCenter
--local uISDefault = Enum.MouseBehavior.Default

local rid = "rbxassetid://"
local clock = os.clock
local sequence = NumberSequence.new
local upper = string.upper
local len = string.len
local Plr = plrs.LocalPlayer
local plrGui = Plr.PlayerGui
local Cam = game.Workspace.CurrentCamera
local efFolder = game.Workspace.Effects
local repS = game:GetService("ReplicatedStorage").Common
local Rems = repS.Remotes
local Input = Rems.Input
local Output = Rems.Output
local ModeEvent = script.ModeEvent

local camera = wS.CurrentCamera
local camCustom = Enum.CameraType.Custom
local camScriptable = Enum.CameraType.Scriptable

local DialogEvent = Rems.Dialog
local isDialog = false
--local DialogList = require(script.Parent.Dialog)
--local enemyCEs = require(script.EnemyClientEvents)
local eLM = repS.EnemyLocalModules
local enemyModules = {}
warn("Amount of enemy modules: " .. tSt(#eLM:GetChildren()))
for i, v in pairs(eLM:GetChildren()) do
	enemyModules[v.Name] = require(v)
	warn("Required " .. v.Name)
end

local v3Lerp = Vector3.new().Lerp

function ara(v) --NOT A JOJO REFERENCE SHUT UP
	return rand(-100,100)/100*v
end

function GetDist(one,two)
	return (two-one).Magnitude
end

local function floorToBase(number,base)
	return floor(number*base)/base
end

--music
local Music = Instance.new("Sound",game.Workspace)
Music.Name = "Music"
Music.Looped = true
Music.SoundId = rid .. "564466195"
Music:Play()
local SendPlaybackLoudness = false
Music.PlaybackSpeed = 1
local coolUpdateCycle = 0

local function omniWait(waitTime)
	local sum = 0
	while sum < waitTime do
		local t, step = runService.Stepped:Wait()
		sum += step
	end
end

------------------------
-- Decorative Set GUI --
------------------------

local Gui = nil
local Title = nil
local TitleShadow = nil
local Vis = nil
local VisTable = {}
local MusicLabel = nil
function makeGui(whichGui)
	Gui = script[whichGui .. "Gui"]:Clone()
	Gui.Parent = plrGui
	Title = Gui.Title
	TitleShadow = Gui:FindFirstChild("TitleShadow")
	Vis = Gui.Visualizer
	local absSize = Vis.AbsoluteSize
	Vis.Position = Vis.Position + uD2(0,absSize.Y-absSize.X,0,0)
	Vis.Size = Vis.Size + uD2(0,absSize.Y-absSize.X,0,0)
	VisTable = {}
	MusicLabel = Gui.Top.Music
	for i = 1, #Vis:GetChildren() do
		local bar = Vis["Bar" .. tSt(i)]
		insert(VisTable,{bar,bar.Position,bar.Size})
	end
end
makeGui("Star")

function changeGuiMode(name,color1,color2,font,artist,track)
	Title.Text = upper(name)
	Title.Font = font
	Title.TextColor3 = color1
	Title.TextStrokeColor3 = color2
	MusicLabel.Text = artist .. " - " .. track
	if TitleShadow then
		TitleShadow.Text = upper(name)
		TitleShadow.Font = font
	end
	for i, v in pairs(VisTable) do
		v[1].BackgroundColor3 = color1
	end
	--adapt to aspect ratio
	local absSize = Vis.AbsoluteSize
	Vis.Position = Vis.Position + uD2(0,absSize.Y-absSize.X,0,0)
	Vis.Size = Vis.Size + uD2(0,absSize.Y-absSize.X,0,0)
end

--Miscellaneous
function CamShake(ins,reps)
	local Hum = Plr.Character.Humanoid
	for i = 1, reps do
		local mult = 1-i/reps
		local goal = v3(ara(ins)*mult,ara(ins)*mult,ara(ins)*mult)
		for j = 1,2 do
			local mult = 1-i/reps
			Hum.CameraOffset = v3Lerp(Hum.CameraOffset,goal,0.5)
			omniWait(0.016)
		end
	end
	local goal = v3(0,0,0)
	for j = 1,2 do
		Hum.CameraOffset = v3Lerp(Hum.CameraOffset,goal,0.5)
		omniWait(0.016)
	end
	Hum.CameraOffset = goal
end

local Notifier = plrGui:WaitForChild("Help")

local tim = nil
local NotifyTinfo1 = TweenInfo.new(0.25,Enum.EasingStyle.Quad,Enum.EasingDirection.Out)
local NotifyTinfo2 = TweenInfo.new(0.5,Enum.EasingStyle.Linear)
local goal1 = {TextTransparency = 0}
local goal2 = {TextTransparency = 1}
function Notify(message)
	tim = os.time()
	local tim2 = tim
	Notifier.Label.Text = message
	if not Notifier.Enabled then
		Notifier.Enabled = true
		twS:Create(Notifier.Label,NotifyTinfo1,goal1):Play()
	end
	wait(2)
	if tim == tim2 then
		twS:Create(Notifier.Label,NotifyTinfo2,goal2):Play()
		wait(1)
		Notifier.Enabled = false
	end
end

local modeKeys = {"Q","E","R","F"}
local abilityKeys = {"Z","X","C","V"}
local otherModeKeys = {"B","N","M","1","2","3","4"}

--------------------
-- Main interface --
--------------------

local mainInterface = plrGui:WaitForChild("MainInterface")
local interfaceCooldowns = mainInterface.Cooldowns
local interfaceDiamond = mainInterface.Diamond
local interfaceMenu = mainInterface.Menu
interfaceDiamond.Visible = false
interfaceMenu.Visible = false

--------------------
-- Cooldown stuff --
--------------------

local coolDown = {}
local movesExist = {Z = nil,X = nil,C = nil,V = nil}

function getCoolTime(tab)
	if not tab then
		return 0
	end
	local t = tab[2]-(clock()-tab[1])
	return t > 0 and floorToBase(t,10) or 0
end

local currentCoolCycle = 0

function updateCooldownGui(Set,Side,Mode,Color)
	--Stop any other cooldown loops currently occurring
	local thisCycle = clock()
	currentCoolCycle = thisCycle
	for i, inp in pairs(abilityKeys) do
		local part = interfaceCooldowns[inp]
		if movesExist[inp] then
			part.Visible = true
			if Color then
				part.Star1.BackgroundColor3 = Color
				part.Star1.Star12.BackgroundColor3 = Color
			end

			--Show the counter
			part.Gradient.Transparency = sequence(1,0.7)
			part.WhiteBarBottom.Visible = true
			part.WhiteBarTop.Visible = true
			part.FramePart.Visible = true
			part.WhiteBarDiagonal.Visible = true
			part.Time.Visible = true

			--Get potential cooldowns
			local cool = nil
			local coolTime = 0

			local cools = {coolDown.Main,coolDown[inp],coolDown[Set .. Side .. Mode .. inp]}

			--Determine the greatest cooldown
			for i, v in pairs(cools) do
				local t = getCoolTime(v)
				if t > coolTime then
					cool = v
					coolTime = t
				end
			end

			coroutine.wrap(function() -- Cool down
				while currentCoolCycle == thisCycle and coolTime > 0 do --Update the cooltime and GUI while it exists
					coolTime = getCoolTime(cool)
					part.Time.Text = coolTime
					wait(0.1)
				end

				if coolTime <= 0 then --Remove the counter (only) once done cooling
					part.Gradient.Transparency = sequence(1)
					part.WhiteBarBottom.Visible = false
					part.WhiteBarTop.Visible = false
					part.FramePart.Visible = false
					part.WhiteBarDiagonal.Visible = false
					part.Time.Visible = false
				end
			end)()
		else
			part.Visible = false
		end
	end
end


function handleCooldown(cool,inp,Set,Side,Mode)
	local now = floorToBase(clock(),10)
	coolDown.Main = {now,cool}
	coolDown[inp] = {now,cool*3}
	coolDown[Set .. Side .. Mode .. inp] = {now,cool*9}
	updateCooldownGui(Set,Side,Mode)
end


----------------------------------
-- Mode switching diamond stuff --
----------------------------------

local storedModeKey = ""

function resizeDiamond()
	local diamondAbs = interfaceDiamond.AbsoluteSize
	local sizeDiff = diamondAbs.Y-diamondAbs.X
	interfaceDiamond.Size = uD2(0.281,sizeDiff,0.5,0)
	interfaceDiamond.Position = uD2(0.359,sizeDiff/(-2),0.25,0)
	local strokeSize = ceil(diamondAbs.Y*0.00926)
	for i = 1, 4 do
		interfaceDiamond["Box" .. tSt(i)].Inside.UIStroke.Thickness = strokeSize
	end
end
resizeDiamond()

function updateDiamond(data)
	for j = 1, 4 do
		local thisBox = interfaceDiamond["Box" .. tSt(j)].Inside
		for i = 1, 4 do
			local thisStar = thisBox["Star" .. tSt(i)]
			--print("Trying to obtain " .. tSt(modeKeys[j]) .. tSt(modeKeys[i]))
			local thisColor = data[modeKeys[j]][modeKeys[i]]
			if thisColor then
				thisStar.BackgroundColor3 = thisColor
				thisStar.Piece1.BackgroundColor3 = thisColor
				thisStar.Visible = true
			else
				thisStar.Visible = false
			end
		end
	end
end

local normalPositions = {uD2(0,0,0,0),uD2(0.5,0,0,0),uD2(0.5,0,0.5,0),uD2(0,0,0.5,0)}
local boxResizes = {{1,1},{0,1},{0,0},{1,0}}
local boxMajorOffsets = {{0,-1},{1,0},{0,1},{-1,0}}
local diamondOffsets = {0.12,0.2}

function resizeBoxes(mainBox,bigSize,smallSize)
	for i = 1, 4 do
		local object = interfaceDiamond["Box" .. tSt(i)]
		local size = (i == mainBox and bigSize) or smallSize --Use the big box size if the box is the main one
		local offset = 0.5-size
		local boxFactors = boxResizes[i]
		object.Size = uD2(size,0,size,0)
		object.Position = normalPositions[i]+uD2(boxFactors[1]*offset,0,boxFactors[2]*offset,0)
	end
	local mainOffsets = boxMajorOffsets[mainBox]
	interfaceDiamond.Position = uD2(0.359-diamondOffsets[1]*mainOffsets[1],0,0.25-diamondOffsets[2]*mainOffsets[2],0) --Offset the main diamond
end

--------------
-- The Menu --
--------------
local menuIsOpen = false
local currentPage = 2 -- 1 is forms, 2 is equips, 3 is summons, 4 is settings/info
local menuInTransit = false
local buttonMouseIsOn = ""
local pages = interfaceMenu.Pages
local mainStar = mainInterface.FatStar
local lineR, lineB = interfaceMenu.RightLine, interfaceMenu.BottomLine
local starR, starB = interfaceMenu.StarTR, interfaceMenu.StarBL
local buttonParent = interfaceMenu.NavButtons
local buttons = {buttonParent.Forms,buttonParent.Equips,buttonParent.Summons,buttonParent.Other}
local buttonTags = {"Forms","Equipment","Enemies","Information and Settings"}
local pageFolder = interfaceMenu.Pages
local pages = {nil,pageFolder.Equipment,nil,nil}
local selectName, selectDetails = interfaceMenu.SelectName, interfaceMenu.SelectDetails
local pageName, nameDecoration = interfaceMenu.PageName, interfaceMenu.NameDecoration
local hoverLabel = mainInterface.HoverLabel
hoverLabel.Visible = false

-----------------------
-- Page 2: Equipment --
-----------------------
local equipModule = require(repS.EquipModule)
local equipDisplay = pages[2].Display
local displaySortType, displaySortDirection = pages[2].SortType, pages[2].SortDirection
local tabWeapons, tabArmor = pages[2].TabWeapons, pages[2].TabArmor
local equipFolder = repS.Equipment
local sEquipName = pages[2].WeaponName -- sEquip is shorthand for "selected equip"
local sEquipDescription = pages[2].WeaponDescription
local sEquipType = pages[2].WeaponType
local sEquipTraits = {pages[2].WeaponTrait1,pages[2].WeaponTrait2,pages[2].WeaponTrait3,pages[2].WeaponTrait4}
local sEquipFrame = pages[2].ItemSelected.ViewportFrame
local sEquipCam = new("Camera")
sEquipCam.CameraType = Enum.CameraType.Scriptable
sEquipCam.Parent = sEquipFrame
sEquipCam.CFrame = CFrame.new(0,10,-10)
sEquipFrame.CurrentCamera = sEquipCam

local spinning = false
local spinAngle = 0
local spinMagnitude = 0
local spinY = 0

local currentEquipCategory = 1
local sEquip = 0

local equipRarityColors = {rgb(255,255,255),rgb(255,246,124),rgb(255,36,50),rgb(216,58,255),rgb(101,59,255),rgb(64,182,255)}
local equipRarityNames = {"Regular", "Advanced", "Elite", "Extraordinary", "Transcendent", "Exalted"}
local equipTraitNames =  {"%attack", "defense", "%health", "%resistance", "speed"}
local equipTraitColors = {"ff0e0e",  "7db4f3",  "5aee7d",  "3155f5",      "eeea5b"}

local currentSortOrder = 1
local sortReverse = false

local function flipArray(array)
	local n = #array
	for i = 1, floor(n/2) do
		local a, b = array[i], array[n + 1 - i]
		array[i] = b
		array[n + 1 - i] = a
	end
end

local function sortRarity(one,two,fallbackSort,fallbackFallback)
	local a, b = one[2], two[2]
	if a < b then
		return false
	elseif a > b or not fallbackSort then
		return true
	else
		return fallbackSort(one,two,fallbackFallback)
	end
end


local function sortClass(one,two,fallbackSort,fallbackFallback)
	local a, b = tn(sub(tSt(one[7]),1,1)), tn(sub(tSt(two[7]),1,1))
	if a > b then
		return false
	elseif a < b or not fallbackSort then
		return true
	else
		return fallbackSort(one,two,fallbackFallback)
	end
end

local function sortAlpabetically(one,two,fallbackSort,fallbackFallback)
	local a, b = one[1], two[1]
	local aN, bN = len(a), len(b)
	local n = (aN <= bN and aN) or bN
	for i = 1, n do
		local A, B = upper(sub(a,i,i)), upper(sub(b,i,i))
		if A < B then
			return true
		elseif A > B then
			return false
		end
	end --At this point, the names are entirely identical until the shortest one ends
	if aN == bN and fallbackSort then
 		return fallbackSort(one,two,fallbackFallback)
	elseif n == aN then -- If a is the shorter one (n), we return it. If the strings are identical, we return a (true) to keep them in their previous order (stable sorting).
		return true
	else
		return false
	end
end

local sortOrders = {
	{"Rarity", {sortRarity,        sortClass,  sortAlpabetically}},
	{"Class",  {sortClass,         sortRarity, sortAlpabetically}},
	{"Name",   {sortAlpabetically, sortRarity, sortClass}}
}

  -----------------------------------------------
--| leftSorter -> leftEnd | rightSorter -> last |--
  -----------------------------------------------
local function merge(array,first,last,sorter1,sorter2,sorter3)
	local leftEnd = floor((first + last)/2) --1
	local leftSorter = first --1
	local rightSorter = leftEnd + 1 -- 2
	local sorted = {unpack(array)}
	for i = first, last do
		if rightSorter > last or (leftSorter <= leftEnd and sorter1(array[leftSorter],array[rightSorter],sorter2,sorter3)) then --Switch last to < to reverse order
			sorted[i] = array[leftSorter]
			leftSorter += 1
		else
			sorted[i] = array[rightSorter]
			rightSorter += 1
		end
	end
	for i = first, last do
		array[i] = sorted[i]
		--print(sorted[i][1])
	end
end

function mergeSort(array,first,last,sorter1,sorter2,sorter3)
	first = first or 1
	last = last or #array

	if first < last then
		local middle = floor((first + last)/2)
		mergeSort(array,first,middle,sorter1,sorter2,sorter3) -- Sort left (recursively)
		mergeSort(array,middle + 1,last,sorter1,sorter2,sorter3) -- Sort right (recursively)

		merge(array,first,last,sorter1,sorter2,sorter3)
	end
end

local format = string.format

function refreshEquips(ownedWeapons)
	local equips = {unpack(equipModule[currentEquipCategory])} --Later this will be only the equips you have unlocked
	local count = #equips
	local rows = ceil(count/4)
	-- Sort the equips
	mergeSort(equips,nil,nil,unpack(sortOrders[currentSortOrder][2]))
	if sortReverse then
		flipArray(equips)
	end
	-- Adjust the canvas size to make all of the frames perfectly square
	local pixelWidth = equipDisplay.Frame.AbsoluteSize.X
	local displayY = (pixelWidth/5)*(rows*6 + 1)
	local displayAbsY = equipDisplay.AbsoluteSize.Y
	equipDisplay.CanvasSize = uD2(0,0,0,displayY)
	--Adjust if there are < 4 rows
	local practicalCanvas = 1
	local offset
	if rows < 4 and displayY <= displayAbsY then
		offset = pixelWidth/5/displayAbsY--offset = 0.0467289
	else
		offset = 1/(rows*6 + 1)
	end
	-- Adjust the frame sizes to make them take up the right amounts of the canvas
	--local offset = practicalCanvas/(rows*6 + 1)
	local height = offset*5
	local diff = offset*6

	for i, v in pairs(equipDisplay:GetChildren()) do
		if v.Name ~= "Frame" then
			v:Destroy()
		end
	end

	local frameName = "WeaponFrame"
	if currentEquipCategory == 2 then
		frameName = "ArmorFrame"
	end

	for i, equip in paris(equips) do
		local row = ceil(i/4) - 1
		local column = i - row*4 - 1

		local f = new("Frame")
		f.Name = frameName
		f.Size = uD2(0.2,0,height,0)
		f.Position = uD2(0.04 + column*0.24,0,offset + row*diff,0)
		f.BorderSizePixel = 0

		local vF = new("ViewportFrame")
		vF.Name = equip[8]
		vF.Size = uD2(1,0,1,0)
		vF.BackgroundTransparency = 1
		vF.Position = uD2(0,0,0,0)
		local w = equipFolder[equip[1]]:Clone()
		w.Parent = vF
		local c = new("Camera")
		c.CFrame = equip[4]
		c.Parent = vF
		vF.CurrentCamera = c
		vF.Parent = f

		local g = new("UIGradient")
		g.Rotation = -45
		g.Transparency = NumberSequence.new(0,1)
		g.Parent = f
		if equip[2] == 6 then
			f.BackgroundColor3 = equipRarityColors[equip[2]]
		else
			f.BackgroundColor3 = equipRarityColors[equip[2]]
		end

		f.Parent = equipDisplay
	end
end

local uv = v3(0,1,0)
function lookAt(start,ende)
	local fv = (ende-start).Unit
	local rv = fv:Cross(uv)
	local uv2 = rv:Cross(fv)
	return CFrame.fromMatrix(start,rv,uv2)
end

function handleEquipClick(equipId,equipCategory,keepSpin)
	local equip = equipModule[equipCategory][equipId]
	local rarityColor = equipRarityColors[equip[2]]
	local hexColor = format("%02X%02X%02X",rarityColor.R*255,rarityColor.G*255,rarityColor.B*255)
	-- Change the information
	sEquipName.Text = equip[1]
	sEquipType.Text = '<font color="#' .. hexColor .. '">' .. equipRarityNames[equip[2]] .. '</font> ' .. equip[3]
	sEquipDescription.Text = equip[5]
	local traits = equip[6]
	for i = 1, 4 do -- Show the traits
		local traitId = traits[i*2 - 1]
		if traitId then
			local amount = traits[i*2]
			local traitName = equipTraitNames[traitId]
			-- Add the sign based on positivity or negativity
			amount = (amount >= 0 and "+ " .. tSt(amount)) or "- " .. tSt(amount)
			-- Add the % if the trait uses % as the measure
			if sub(traitName,1,1) == "%" then
				amount = amount .. "%"
				traitName = sub(traitName,2,len(traitName))
			end
			sEquipTraits[i].Text = '<font color="#' .. equipTraitColors[traitId] .. '">' .. amount .. '</font> ' .. traitName
		else
			sEquipTraits[i].Text = ""
		end
	end
	-- Replace the existing model
	local model = sEquipFrame:FindFirstChildOfClass("Model")
	if model then
		model:Destroy()
	end
	model = equipFolder:FindFirstChild(equip[1])
	if model then
		model:Clone().Parent = sEquipFrame
	end
	-- Adjust spin values
	if not keepSpin then
		spinAngle = 0
	end
	spinY = equip[4].Position.Y
	spinMagnitude = v3(equip[4].Position.X,0,equip[4].Position.Z).Magnitude
end

local mTInfo1 = TweenInfo.new(0.25,Enum.EasingStyle.Quad,Enum.EasingDirection.Out)
local mTInfo2 = TweenInfo.new(0.25,Enum.EasingStyle.Quad,Enum.EasingDirection.In)
function openMenu()
	refreshEquips()
	handleEquipClick(1,1,true)
	menuInTransit = true

	interfaceMenu.BackgroundTransparency = 1 --Set the stage
	lineB.Position = uD2(1,0,0.998,0) --Push the stars and lines to the corner
	lineB.Size = uD2(0,0,0.005,0)
	lineR.Position = uD2(0.998,0,1,0)
	lineR.Size = uD2(0.003,0,0,0)
	starB.Position = uD2(0.994,0,0.989,0)
	starR.Position = uD2(0.994,0,0.989,0)
	for i, v in pairs(buttons) do
		v.BackgroundTransparency = 1
		v.NavButton.ImageTransparency = 1
	end
	selectName.TextTransparency = 1
	selectName.TextStrokeTransparency = 1
	selectDetails.TextTransparency = 1
	selectDetails.TextStrokeTransparency = 1

	pageName.TextTransparency = 1
	pageName.TextStrokeTransparency = 1
	nameDecoration.ImageTransparency = 1
	nameDecoration.Frame.BackgroundTransparency = 1
	nameDecoration.Frame.Image.ImageTransparency = 1

	local children = pages[currentPage]:GetChildren()
	for i, v in pairs(children) do
		v.Visible = false
	end

	twS:Create(mainStar,mTInfo1,{Position = uD2(0.854,0,0.803,0), Size = uD2(0.042,0,0.075,0)}):Play()
	wait(0.25)

	interfaceMenu.Visible = true --The curtain rises

	twS:Create(lineB,mTInfo1,{Position = uD2(0,0,0.998,0), Size = uD2(1,0,0.005,0)}):Play()
	twS:Create(lineR,mTInfo1,{Position = uD2(0.998,0,0,0), Size = uD2(0.003,0,1,0)}):Play()
	twS:Create(starB,mTInfo1,{Position = uD2(-0.006,0,0.989,0)}):Play()
	twS:Create(starR,mTInfo1,{Position = uD2(0.994,0,-0.01,0)}):Play()
	wait(0.25)
	twS:Create(interfaceMenu,mTInfo1,{BackgroundTransparency = 0.5}):Play()
	for i, v in pairs(buttons) do
		twS:Create(v,mTInfo1,{BackgroundTransparency = 0.5}):Play()
		twS:Create(v.NavButton,mTInfo1,{ImageTransparency = 0}):Play()
	end
	twS:Create(pageName,mTInfo1,{TextTransparency = 0, TextStrokeTransparency = 0.85}):Play()
	twS:Create(nameDecoration,mTInfo1,{ImageTransparency = 0.5}):Play()
	twS:Create(nameDecoration.Frame,mTInfo1,{BackgroundTransparency = 0.5}):Play()
	twS:Create(nameDecoration.Frame.Image,mTInfo1,{ImageTransparency = 0.5}):Play()
	for i, v in pairs(children) do --Show the page elements
		v.Visible = true
		wait()
	end
	menuIsOpen = true
	menuInTransit = false
	while menuIsOpen do -- MAJOR SPOT FOR CLEANUP
		if currentPage == 2 then
			local x, y = sin(rad(spinAngle)), cos(rad(spinAngle))
			sEquipCam.CFrame = lookAt(v3(x*spinMagnitude,spinY,y*spinMagnitude),v3(0,0,0))
			spinAngle += 2
			wait()
		end
		local mPos = uIS:GetMouseLocation() - Vector2.new(0,26)
		--print(mPos.Y)
		local gObjects = plrGui:GetGuiObjectsAtPosition(mPos.X,mPos.Y)
		local thisButton = ""
		local sawWeaponFrame = false
		for i, v in pairs(gObjects) do
			--print(v.Name)
			--print(v.Name == "NavButton")
			--hoverLabel.Visible = false
			if v.Name == "NavButton" then
				thisButton = v.Parent
				print(thisButton.Name)
			elseif v.Name == "WeaponFrame" then
				hoverLabel.Text = equipModule[currentEquipCategory][tn(v:FindFirstChildOfClass("ViewportFrame").Name)][1]
				hoverLabel.Position = uD2(0,mPos.X+5,0,mPos.Y)
				hoverLabel.Visible = true
				sawWeaponFrame = true
			end
		end
		if not sawWeaponFrame then
			hoverLabel.Visible = false
		end
		if thisButton ~= buttonMouseIsOn then
			-- Shrink formerly emphasized button
			if buttonMouseIsOn ~= "" then
				local size = buttonMouseIsOn.Size
				local position = buttonMouseIsOn.Position
				local sX, sY = size.X.Scale, size.Y.Scale
				--local pX, pY = position.X.Scale+sX*0.1, position.Y.Scale+sY*0.1
				twS:Create(buttonMouseIsOn,mTInfo1,{Position = uD2(-0.25 + tFind(buttons,buttonMouseIsOn)*0.25,0,0,0), Size = uD2(0.21,0,1,0)}):Play()
			else
				selectName.Text = buttonTags[tFind(buttons,thisButton)]
				twS:Create(selectName,mTInfo1,{TextTransparency = 0, TextStrokeTransparency = 0.85}):Play()
			end
			buttonMouseIsOn = thisButton
			-- Enlarge newly emphasized button
			if buttonMouseIsOn ~= "" then
				local size = buttonMouseIsOn.Size
				local position = buttonMouseIsOn.Position
				local sX, sY = size.X.Scale, size.Y.Scale
				--local pX, pY = position.X.Scale-sX*0.125, position.Y.Scale-sY*0.125
				twS:Create(buttonMouseIsOn,mTInfo1,{Position = uD2(-0.271 + tFind(buttons,buttonMouseIsOn)*0.25,0,0,0), Size = uD2(0.252,0,1.2,0)}):Play()
			else
				twS:Create(selectName,mTInfo1,{TextTransparency = 1, TextStrokeTransparency = 1}):Play()
			end
		end
		wait()
	end
end

function closeMenu()
	menuInTransit = true
	twS:Create(interfaceMenu,mTInfo2,{BackgroundTransparency = 1}):Play()
	for i, v in pairs(buttons) do
		twS:Create(v,mTInfo2,{BackgroundTransparency = 1}):Play()
		twS:Create(v.NavButton,mTInfo2,{ImageTransparency = 1}):Play()
	end
	twS:Create(pageName,mTInfo2,{TextTransparency = 1, TextStrokeTransparency = 1}):Play()
	twS:Create(nameDecoration,mTInfo2,{ImageTransparency = 1}):Play()
	twS:Create(nameDecoration.Frame,mTInfo2,{BackgroundTransparency = 1}):Play()
	twS:Create(nameDecoration.Frame.Image,mTInfo2,{ImageTransparency = 1}):Play()
	local children = pages[currentPage]:GetChildren() --Hide the page elements
	local when = clock() + 0.25
	for i, v in pairs(children) do
		v.Visible = false
		wait()
	end
	wait(when-clock())
	twS:Create(lineB,mTInfo2,{Position = uD2(1,0,0.998,0), Size = uD2(0,0,0.005,0)}):Play()
	twS:Create(lineR,mTInfo2,{Position = uD2(0.998,0,1,0), Size = uD2(0.003,0,0,0)}):Play()
	twS:Create(starB,mTInfo2,{Position = uD2(0.994,0,0.989,0)}):Play()
	twS:Create(starR,mTInfo2,{Position = uD2(0.994,0,0.989,0)}):Play()
	wait(0.25)
	twS:Create(mainStar,mTInfo2,{Position = uD2(0,106,0,7), Size = uD2(0,26,0,26)}):Play()
	wait(0.25)
	interfaceMenu.Visible = false --The curtain falls
	menuIsOpen = false
	menuInTransit = false
end

function changeSortType()
	currentSortOrder = (currentSortOrder == 3 and 1) or currentSortOrder + 1
	displaySortType.Text = "Sort by: " .. sortOrders[currentSortOrder][1]
	refreshEquips()
end

function changeSortDirection()
	if sortReverse then
		sortReverse = false
		displaySortDirection.Rotation = 0
	else
		sortReverse = true
		displaySortDirection.Rotation = 180
	end
	refreshEquips()
end

function changeEquipCategory(armor)
	if armor and currentEquipCategory ~= 2 then
		currentEquipCategory = 2
		tabArmor.BackgroundTransparency = 0.7
		tabArmor.TextColor3 = rgb(255,255,255)
		tabWeapons.BackgroundTransparency = 0.6
		tabWeapons.TextColor3 = rgb(171,171,171)
		refreshEquips()
	elseif not armor and currentEquipCategory ~= 1 then
		currentEquipCategory = 1
		tabWeapons.BackgroundTransparency = 0.7
		tabWeapons.TextColor3 = rgb(255,255,255)
		tabArmor.BackgroundTransparency = 0.6
		tabArmor.TextColor3 = rgb(171,171,171)
		refreshEquips()
	end
end

function handleMenuClick()
	local mPos = uIS:GetMouseLocation() - Vector2.new(0,26)
	local gObjects = plrGui:GetGuiObjectsAtPosition(mPos.X,mPos.Y)
	for i, v in pairs(gObjects) do
		if v.Name == "WeaponFrame" then
			handleEquipClick(tn(v:FindFirstChildOfClass("ViewportFrame").Name),1)
		elseif v.Name == "ArmorFrame" then
			handleEquipClick(tn(v:FindFirstChildOfClass("ViewportFrame").Name),2)
		elseif v == displaySortType then
			changeSortType()
		elseif v == displaySortDirection then
			changeSortDirection()
		elseif v == tabWeapons then
			changeEquipCategory()
		elseif v == tabArmor then
			changeEquipCategory(true)
		end
	end
end


--Camera stuff
--camera.CameraType = camScriptable
--uIS.MouseBehavior = uISLockedCenter

--Dialog stuff that's here from the SG:RPG, planning to rewrite it ~~if~~ WHEN we add NPCs to the map

--[[
diaCache = {}
function createDialog(f,tab)
	if tab.Fire then
		DialogEvent:FireServer(tab.Fire)
	end
	if tab.c ~= 0 then
		f.SF.CanvasSize = UDim2.new(0,0,0,80+(tab.c-1)*90)
		for i = 1, tab.c do
			local c = f.SF["0"]:Clone()
			c.TB.Text = tab[i].Choice
			c.Visible = true
			c.Name = i
			c.Position = UDim2.new(0,0,0,90*(i-1))
			c.Parent = f.SF
			--Make the box show the cool gradient when the mouse enters it.
			diaCache[#diaCache+1] = c.TB.MouseEnter:Connect(function()
				for i = 1, 8 do
					c.BackgroundTransparency = c.BackgroundTransparency/2
					runService.Heartbeat:Wait()
				end
				c.BackgroundTransparency = 0
			end)
			--Make the box fade out when the mouse leaves.
			diaCache[#diaCache+1] = c.TB.MouseLeave:Connect(function()
				for i = 1, 8 do
					c.BackgroundTransparency = (c.BackgroundTransparency+1)/2
					runService.Heartbeat:Wait()
				end
				c.BackgroundTransparency = 1
			end)
			--Handle button click transitions.
			diaCache[#diaCache+1] = c.TB.Activated:Connect(function()

				f.Words.Text = tab[i].Resp
				if string.len(tab[i].Resp) > 100 then
					f.Words.TextScaled = true
				else
					f.Words.TextScaled = false
				end

				for i, v in pairs(f.SF:GetChildren()) do
					if v.Name ~= "0" then
						v:Destroy()
					end
				end
				for i, v in pairs(diaCache) do
					v:Disconnect()
				end
				diaCache = {}
				createDialog(f,tab[i])
			end)
		end
	else
		wait(1+string.len(f.Words.Text)*0.03)
		for i = 1, 20 do
			pcall(function()
				f.Position = f.Position:Lerp(UDim2.new(0,100,1,0),0.3)
				runService.Heartbeat:Wait()
			end)
		end
		pcall(function() f.Parent:Destroy() end)
		isDialog = false
	end
end

function Dialog(strin,model)
	isDialog = true
	local g = script.Dialog:Clone()
	g.Parent = Plr.PlayerGui
	local f = g.Frame

	f.NPC.Text = strin.NPC
	f.Words.Text = strin.FT
	if string.len(strin.FT) > 100 then
		f.Words.TextScaled = true
	end

	createDialog(f,strin)

	local c = model:Clone()
	c.Parent = f.VF
	local cam = Instance.new("Camera",f.VF)
	f.VF.CurrentCamera = cam
	cam.CFrame = c.ClickBox.CFrame*strin.C

	g.Frame.Position = UDim2.new(0,100,1,0)
	g.Enabled = true
	for i = 1, 20 do
		f.Position =f.Position:Lerp(UDim2.new(0,100,0.6,0),0.3)
		runService.Heartbeat:Wait()
	end
	g.Frame.Position = UDim2.new(0,100,0.6,0)
end
]]
local shouldStartPBLLoop = true
local connectionTable = {}

local ngh = script.sgAnimate
function giveScript(plr,mode)
	if not script:FindFirstChild(plr.Name) then
		local c = ngh:Clone()
		c.Name = plr.Name
		c.Parent = script
		c.Disabled = false
		if mode then
			local uId = plr.UserId
			local thisOne = rand(1,99999)
			while connectionTable[thisOne] do
				thisOne = rand(1,99999)
			end
			connectionTable[thisOne] = ModeEvent.Event:Connect(function(typ,aUId)
				if typ == "Request" and aUId == uId then
					connectionTable[thisOne]:Disconnect()
					ModeEvent:Fire("Reply",uId,mode)
				end
			end)
		end
	end
end

local hitParams = RaycastParams.new()
hitParams.FilterDescendantsInstances = {efFolder,Plr.Character}
hitParams.FilterType = Enum.RaycastFilterType.Blacklist
local function mouse()
	hitParams.FilterDescendantsInstances = {efFolder,Plr.Character}
	local mouseLocation = uIS:GetMouseLocation()
	local unitRay = Cam:ViewportPointToRay(mouseLocation.X,mouseLocation.Y)
	local cast = workspace:Raycast(unitRay.Origin,unitRay.Direction*1000,hitParams)
	if cast then
		return cast.Position,cast.Normal,cast.Instance
	else
		return unitRay.Origin+unitRay.Direction*1000,v3(0,0,0), nil

	end
end

Output.OnClientEvent:Connect(function(event,arg1,arg2,arg3,arg4,arg5,arg6)
	if event == "GiveScript" then
		giveScript(arg1,arg2)
	elseif event == "ModeChange" and arg1 == Plr.UserId then
		Music.SoundId = rid .. arg2.music[1]
		changeGuiMode(arg2.name,arg2.main,arg2.sec,arg2.font,arg2.music[2],arg2.music[3])
		if arg2.sendpbl and shouldStartPBLLoop then
			shouldStartPBLLoop = false
			SendPlaybackLoudness = true
			while SendPlaybackLoudness do
				wait(0.1)
				Input:FireServer("PBL",Music.PlaybackLoudness)
			end
			shouldStartPBLLoop = true --This makes it so that multiple loops are not created if a player switches from one PBL mode to another
		else
			SendPlaybackLoudness = false
		end
	--elseif event == "Shake" then
		--CamShake(arg1,arg2)
	elseif event == "Notify" then
		Notify(arg1)
	elseif event == "Cooldown" then
		handleCooldown(arg1,arg2,arg3,arg4,arg5,arg6)
	elseif event == "MovesExist" then
		movesExist = arg1
		updateCooldownGui(arg2,arg3,arg4,arg5)
	elseif event == "SideColors" then
		updateDiamond(arg1)
	elseif event == "EnemyInit" then
		warn("Got enemy init message: " .. arg1)
		enemyModules[arg1]["Init"](arg2)
	elseif event == "EnemyClientEvent" then
		--print("Got enemy client event of " .. arg1 .. " " .. arg2)
		enemyModules[arg1][arg2](arg3,arg4,arg5)
	elseif event == "MouseRequest" or (event == "ClientEvent" and arg2 == "BeamUpdate" and arg1 == Plr.UserId) then
		--print("Client got mouserequest, updating")
		Input:FireServer("Input",nil,mouse())
	end
end)



--Inputs
function handleModeInput(key)
	storedModeKey = storedModeKey .. key
	if len(storedModeKey) >= 2 then
		--print("Firing server with " .. storedModeKey)
		Input:FireServer("Input",storedModeKey,mouse())
		storedModeKey = ""
		interfaceDiamond.Visible = false
	else
		interfaceDiamond.Visible = true
		for i = 1, 4 do
			local thisBox = interfaceDiamond["Box" .. tSt(i)].Inside
			if i == tFind(modeKeys,key) then
				thisBox.Key.TextTransparency = 0
			else
				thisBox.Key.TextTransparency = 0.75
			end
		end
		resizeBoxes(tFind(modeKeys,key),0.6,0.4)
	end
end
uIS.InputBegan:Connect(function(k,object)
	local keyCode = k.KeyCode
	if --[[not object and]] k.UserInputType == Enum.UserInputType.MouseButton1 and not menuInTransit then
		if menuIsOpen then
			handleMenuClick()
		else
			Input:FireServer("Input","click",mouse())
		end
	elseif not object and keyCode and not menuInTransit then
		keyCode = sub(tSt(keyCode),14)
		--print("Got key " .. keyCode)
		if keyCode == "G" then
			if menuIsOpen then
				closeMenu()
			else
				openMenu()
			end
		elseif tFind(modeKeys,keyCode) and not menuIsOpen then
			--print("Got mode key")
			handleModeInput(keyCode)
		elseif tFind(abilityKeys,keyCode) or tFind(otherModeKeys,keyCode) and not menuIsOpen then
			Input:FireServer("Input",keyCode,mouse())
		end
	end
end)


wait()
--[[
for i, v in pairs (plrs:GetChildren()) do
	giveScript(v.Name)
end

plrs.PlayerAdded:Connect(function(plr)
	giveScript(plr.Name)
end)
]]

--hey
Input:FireServer("Message",Plr.Name.."'s local script has loaded.")

--More legacy stuff from the SG:RPG
--[[
Plr.CharacterAdded:Connect(function() --Handle the health bar (it has to get the humanoid every time the character respawns
	isDialog = false
	wait(0.5)
	hum = Plr.Character.Humanoid
	G.Box.HP.L.Text = tostring(hum.MaxHealth) .. "/" .. tostring(hum.MaxHealth)
	pcall(function() conch:Disconnect() end)
	conch = hum.HealthChanged:Connect(function()
		print(tostring(math.floor(hum.Health)))
		G.Box.HP.L.Text = tostring(math.floor(hum.Health)) .. "/" .. tostring(hum.MaxHealth)
		repeat
			G.Box.HP.B.Size = G.Box.HP.B.Size:Lerp(UDim2.new(hum.Health/hum.MaxHealth,0,0,13),0.1)
			runService.Heartbeat:Wait()
		until G.Box.HP.B.Size == UDim2.new(hum.Health/hum.MaxHealth,0,0,13) or not hum
	end)
end)

repeat ---Get the gosh darn humanoid already
	pcall(function() hum = Plr.Character.Humanoid end)
	wait()
until hum

if not conch then --The script's not fast enough to detect the initial spawn so I'm just leaving this here
	conch = hum.HealthChanged:Connect(function()
		print(tostring(math.floor(hum.Health)))
		G.Box.HP.L.Text = tostring(math.floor(hum.Health)) .. "/" .. tostring(hum.MaxHealth)
		repeat
			G.Box.HP.B.Size = G.Box.HP.B.Size:Lerp(UDim2.new(hum.Health/hum.MaxHealth,0,0,13),0.1)
			runService.Heartbeat:Wait()
		until G.Box.HP.B.Size == UDim2.new(hum.Health/hum.MaxHealth,0,0,13) or not hum
	end)
end
]]
