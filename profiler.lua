--An attempt to profile the speed of TweenService:GetValue
local TweenService = game:GetService("TweenService")
local clock = os.clock
local first = clock()
for i = 1, 500 do
    local alpha = TweenService:GetValue(20/6, Enum.EasingStyle.Linear, Enum.EasingDirection.InOut)
end
local second = clock()
print(second-first)  